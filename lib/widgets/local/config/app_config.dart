class AppConfig {
  // App Config
  static final List supportedLanguage = ['en', 'ar', 'fr'];
  static const String defaultLanguage = 'en';
  // End App Config
}
