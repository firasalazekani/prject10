import 'package:flutter/material.dart';

class WaveClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    debugPrint(size.width.toString());
    var path = new Path();
    path.lineTo(0.0, size.height - 0);
    //-    +
    //+    -
    // var firstStar = Offset((size.width - 200) / 5, size.height);

    //var firstStar = Offset((size.width - 100) / 5, size.height);
    var firstStar = Offset((size.width) / 7.3, size.height);

    //var firstEnd = Offset(((size.width - 290) / 2.25) + 5, size.height - 100.0);

    //var firstEnd = Offset(((size.width - 220) / 2) + 5, size.height - 100);
    var firstEnd = Offset(size.width / 5, size.height / 1.4);

    path.quadraticBezierTo(
        firstStar.dx, firstStar.dy, firstEnd.dx, firstEnd.dy);
    print(
        'p1 : ${firstStar.dx} p2 : ${firstStar.dy} P3 : ${firstEnd.dx} p4 :${firstEnd.dy}');

    //  var SecondStar =
    //    Offset(size.width - 200 - (size.width / 3.24), size.height - 245);
    var SecondStar =
        // Offset(size.width - 150 - (size.width / 3.24), size.height - 200);
        Offset(size.width / 1.7 - (size.width / 3.24), size.height / 4.5);
    // var SecondEnd = Offset(size.width - 150, size.height - 263);
    var SecondEnd =
        // Offset(size.width - 150, size.height - 198);
        Offset(size.width / 1.7, size.height / 3.9);
    path.quadraticBezierTo(
        SecondStar.dx, SecondStar.dy, SecondEnd.dx, SecondEnd.dy);
    print(
        'p1 : ${SecondStar.dx} p2 : ${SecondStar.dy} P3 : ${SecondEnd.dx} p4 :${SecondEnd.dy}');

    //var thirdStar = Offset((size.width / 5) + 214, size.height - 262.5);
    var thirdStar =
        //Offset((size.width / 5) + 220, size.height - 190);
        Offset(size.width / 1.2, size.height / 3.5);
    //var thirdEnd = Offset((size.width) / 2.25, size.height - 150.0);
    // var thirdEnd = Offset((size.width / 2.25) + 215, size.height - 350);
    var thirdEnd =
        //Offset((size.width / 3) + 260, size.height - 275);
        Offset(size.width, size.height / 50);

    path.quadraticBezierTo(
        thirdStar.dx, thirdStar.dy, thirdEnd.dx, thirdEnd.dy);
    // thirdStar.dx + 208, thirdStar.dy - 220, thirdEnd.dx + 238, 0);
    // path.quadraticBezierTo(290, 80, 420, 0);
    print(
        'p1 : ${thirdStar.dx} p2 : ${thirdStar.dy} P3 : ${thirdEnd.dx} p4 :${thirdEnd.dy}');

    // var FourthStar =
    //     Offset(size.width + 200 + (size.width / 3.24), size.height - 205);
    // var FourthEnd = Offset(size.width, size.height + 10);
    // // path.quadraticBezierTo(
    // //     FourthStar.dx, FourthStar.dy, FourthEnd.dx, FourthEnd.dy);
    // path.quadraticBezierTo(
    //     FourthStar.dx, FourthStar.dy, FourthEnd.dx, FourthEnd.dy);
    // print(
    //     'p1 : ${FourthStar.dx} p2 : ${FourthStar.dy} P3 : ${FourthEnd.dx} p4 :${FourthEnd.dy}');
    // var thirdStar =
    //     Offset(size.width - 200 - (size.width / 6.24), size.height - 205);
    // var thirdEnd = Offset(size.width, size.height - 10);
    // path.quadraticBezierTo(thirdStar.dx, thirdStar.dy, thirdEnd.dx, 0);

    path.lineTo(size.width, 0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return false;
  }
}

class WaveClipperUnder extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    debugPrint(size.width.toString());
    var path = new Path();
    // path.moveTo(0, 150);

    path.moveTo(0, size.height / 1.5);
    // path.quadraticBezierTo(size.width / 1.5, (size.height + 300) / 2,
    //     size.width, size.height - 500 / 2);
    path.quadraticBezierTo(
        //   size.width / 1.5, (size.height) / 0.9, size.width, size.height / 20);
        size.width / 1.2,
        (size.height) / 1,
        size.width,
        size.height / 30);
    path.lineTo(size.width, size.width);
    //زاوية اليسرى من اسفل شاشة
    path.lineTo(0, size.height);

    // path.lineTo(800, size.height - 700);
    // var firstStar = Offset((size.width - 200) / 5, size.height);
    // var firstEnd = Offset(((size.width - 290) / 2.25) + 5, size.height - 100.0);
    // path.quadraticBezierTo(
    //     firstStar.dx, firstStar.dy, firstEnd.dx, firstEnd.dy);
    // print(
    //     'p1 : ${firstStar.dx} p2 : ${firstStar.dy} P3 : ${firstEnd.dx} p4 :${firstEnd.dy}');

    // var SecondStar =
    //     Offset(size.width - 200 - (size.width / 3.24), size.height - 245);
    // var SecondEnd = Offset(size.width - 150, size.height - 263);
    // path.quadraticBezierTo(
    //     SecondStar.dx, SecondStar.dy, SecondEnd.dx, SecondEnd.dy);
    // print(
    //     'p1 : ${SecondStar.dx} p2 : ${SecondStar.dy} P3 : ${SecondEnd.dx} p4 :${SecondEnd.dy}');

    // var thirdStar = Offset((size.width / 5) + 214, size.height - 262.5);
    // //var thirdEnd = Offset((size.width) / 2.25, size.height - 150.0);
    // var thirdEnd = Offset((size.width / 2.25) + 215, size.height - 350);
    // path.quadraticBezierTo(
    //     thirdStar.dx, thirdStar.dy, thirdEnd.dx, thirdEnd.dy);
    // // thirdStar.dx + 208, thirdStar.dy - 220, thirdEnd.dx + 238, 0);
    // // path.quadraticBezierTo(290, 80, 420, 0);
    // print(
    //     'p1 : ${thirdStar.dx} p2 : ${thirdStar.dy} P3 : ${thirdEnd.dx} p4 :${thirdEnd.dy}');

    // var FourthStar =
    //     Offset(size.width + 200 + (size.width / 3.24), size.height - 205);
    // var FourthEnd = Offset(size.width, size.height + 10);
    // // path.quadraticBezierTo(
    // //     FourthStar.dx, FourthStar.dy, FourthEnd.dx, FourthEnd.dy);
    // path.quadraticBezierTo(
    //     FourthStar.dx, FourthStar.dy, FourthEnd.dx, FourthEnd.dy);
    // print(
    //     'p1 : ${FourthStar.dx} p2 : ${FourthStar.dy} P3 : ${FourthEnd.dx} p4 :${FourthEnd.dy}');
    // var thirdStar =
    //     Offset(size.width - 200 - (size.width / 6.24), size.height - 205);
    // var thirdEnd = Offset(size.width, size.height - 10);
    // path.quadraticBezierTo(thirdStar.dx, thirdStar.dy, thirdEnd.dx, 0);

    // path.lineTo(size.width, 0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return false;
  }
}

class WaveClipperButton extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    debugPrint(size.width.toString());
    var path = new Path();

    path.moveTo(0, size.height);
    path.quadraticBezierTo(
        size.width / 4, (size.height / 1.08), size.width / 4, size.height / 2);
    path.quadraticBezierTo(size.width / 4, (size.height) / 12, size.width / 1.6,
        size.height - size.height);

    //زاوية اليسرى من اسفل شاشة
    //هون رسم خط من اعلى الزاوية للانحناء الاعلى الى اخر الشاشة
    path.lineTo(size.width, 0);
    //رسم خط من اخر نقطة تركها المنحي كواداتكبيزير الى مهاية الكونتينر من اسفل
    path.lineTo(size.width, size.height);

    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return false;
  }
}
