import 'package:flutter/material.dart';

//هاد نموذج الزر فيني استخدمه وين ما بدي
class MyBotton extends StatelessWidget {
  MyBotton(
      {required this.title,
      required this.onPressed,
      required this.color,
      required this.colorEnd,
      required this.colorStart});

  final List<Color> color;
  final Color colorEnd;
  final Color colorStart;
  final String title;
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    return
        // Padding(
        //   padding:
        //    const EdgeInsets.symmetric(horizontal: 10),
        // child:
        Material(
      //elevation: 5,
      // color: color,
      borderRadius: BorderRadius.circular(80),
      child: MaterialButton(
        onPressed: onPressed,
        //  minWidth: 500,
        //  height: 42,
        // child:

        child: Container(
          decoration: BoxDecoration(
            //   color: Colors.teal,
            gradient: LinearGradient(
              colors: [colorStart, colorEnd],
              begin: Alignment.bottomLeft,
              end: Alignment.topRight,
            ),
            borderRadius: BorderRadius.circular(30),
            boxShadow: [
              BoxShadow(
                //color: Colors.red,
                color: Colors.grey.withOpacity(0.6),

                blurRadius: 4,
                offset: Offset(0, 5), // Shadow position
              ),
            ],
          ),
          child: Ink(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [colorStart, colorEnd],
                begin: Alignment.bottomLeft,
                end: Alignment.topRight,
              ),
              borderRadius: BorderRadius.circular(30.0),
              // boxShadow: [
              //   BoxShadow(
              //     color: Colors.grey.withOpacity(0.5),
              //     // spreadRadius: 0,
              //     //  blurRadius: 0,
              //     offset: Offset(8, 8),
              //   )
              // ] // changes position of shadow
            ),
            // borderRadius: BorderRadius.only(
            //     topLeft: Radius.circular(30),
            //     bottomRight: Radius.circular(30)),

            child: Container(
              constraints: BoxConstraints(maxWidth: 250.0, minHeight: 40.0),
              alignment: Alignment.center,
              child: Text(
                title,
                style: TextStyle(color: Colors.white, fontSize: 20),
              ),
            ),
          ),
        ),
      ),
      //   ),
    );
  }
}
