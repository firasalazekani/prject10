//import 'dart:ffi';
//import 'dart:js_util';
//import 'dart:html';

import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:regexpattern/regexpattern.dart';

class login {
  String errorMessage = '';
  String FeildRequred = '';
  bool isVaild = false;

  bool validateEmailDomain(String value) {
    String pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    return (!regex.hasMatch(value)) ? false : true;
  }

  String validateEmail(String _strEmail) {
    if (_strEmail.isEmpty) {
      errorMessage = "This field cannot be empty";
      isVaild = false;
      print('_errorMessage :$errorMessage');

      return errorMessage;
    } else {
      // if (!EmailValidator.validate(_strEmail, true)) {
      if (!validateEmailDomain(_strEmail)) {
        errorMessage = "Invalid Email Address Example: example@example.com";
        isVaild = false;
        print('_errorMessage :$errorMessage');

        return errorMessage;
      } else {
        errorMessage = "";
        isVaild = true;
        print('_errorMessage :$errorMessage');

        return errorMessage;
      }
    }
  }

  bool validatePasswordDomain(String value) {
    String pattern =
        r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$';
    RegExp regex = new RegExp(pattern);
    return (!regex.hasMatch(value)) ? false : true;
  }

  String validatePassword(String _strPassword) {
    if (_strPassword.isEmpty) {
      errorMessage = "This field cannot be empty";
      isVaild = false;
      return errorMessage;
    } else {
      if (_strPassword.characters.length < 8) {
        errorMessage = "Less characters 8";
        isVaild = false;
        return errorMessage;
      }
      //else {
      if (!validatePasswordDomain(_strPassword)) {
        errorMessage =
            "The password must contain uppercase and lowercase letters, numbers and symbols";
        isVaild = false;
        return errorMessage;
      } else {
        errorMessage = "";
        isVaild = true;
        return errorMessage;
      }
      //  }
    }
  }

  String fullname = '';

  String FieldRequered(String em, String pa) {
    if (em == "" || pa == "") {
      errorMessage = "The Email and Password fields are required";
      isVaild = false;
      return errorMessage;
    } else {
      errorMessage = "";
      isVaild = true;
      return errorMessage;
    }
  }
}
