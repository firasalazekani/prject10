import 'package:fluster/fluster.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:custom_info_window/custom_info_window.dart';

/// [Fluster] can only handle markers that conform to the [Clusterable] abstract class.
///
/// You can customize this class by adding more parameters that might be needed for
/// your use case. For instance, you can pass an onTap callback or add an
/// [InfoWindow] to your marker here, then you can use the [toMarker] method to convert
/// this to a proper [Marker] that the [GoogleMap] can read.
///
class MapMarker extends Clusterable {
  //
  CustomInfoWindowController _customInfoWindowController =
      CustomInfoWindowController();

  final String id;
  final LatLng position;
  BitmapDescriptor? icon;
  final String? title;
  final String? snippet;
  final VoidCallback? onTop;

  MapMarker({
    required this.id,
    required this.position,
    this.icon,
    isCluster = false,
    clusterId,
    pointsSize,
    childMarkerId,
    InfoWindow,
    this.title,
    this.snippet,
    this.onTop,
  }) : super(
          markerId: id,
          latitude: position.latitude,
          longitude: position.longitude,
          isCluster: isCluster,
          clusterId: clusterId,
          pointsSize: pointsSize,
          childMarkerId: childMarkerId,
          //  InfoWindow:InfoWindow,
          //onTop:onTop,
        );

  Marker toMarker() => Marker(
        markerId: MarkerId(isCluster! ? 'cl_$id' : id),
        position: LatLng(
          position.latitude,
          position.longitude,
        ),
        onTap: onTop,
        icon: icon!,
      );
}
