import 'dart:convert';
import 'dart:io';
import 'dart:async';
import 'package:clustr_map_test_1/Screens/notifaction/notifaction_screen.dart';
import 'package:clustr_map_test_1/Screens/notifaction/notifaction_server.dart';
import 'package:clustr_map_test_1/servies/local/translation_service.dart';
import 'package:clustr_map_test_1/widgets/local/config/app_config.dart';
import 'package:flutter/material.dart';
import 'Screens/home_screen.dart';
import 'package:http/http.dart' as http;
//import 'pages/test_json.dart';

import 'package:clustr_map_test_1/Screens/chat/signin_screen.dart';
import 'package:clustr_map_test_1/Screens/chat/welcome_screen.dart';
import 'package:clustr_map_test_1/Screens/chat/registration_screen.dart';
import 'package:clustr_map_test_1/Screens/chat/chat_screen.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:clustr_map_test_1/Screens/chat/bolt_screen.dart';

import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:rxdart/rxdart.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  ////////////////////////////////////////////
//مهمة هذا براميتر انو يجب البيانات من نوتيفكيشن
  // Future backgroundMessage(RemoteMessage masseging) async {
  //   print("========== backgroundMessage===========");
  //   print("${masseging.notification?.body}");
  // }

  //هذا الميثود يتم اضافته في اعلى الشجرة اي في المين
  //main
  //ويكون في حالة ال
  //background and tremial
//الفائدة من هذا الميثود اذا بدي اعمل اي شيء على داتا بيز
  // FirebaseMessaging.onBackgroundMessage((backgroundMessage));
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final BehaviorSubject<Locale?> _notifierLocale = BehaviorSubject<Locale?>();
  Future? _futureFun;
  Locale _currentLocale = const Locale(AppConfig.defaultLanguage, '');

  @override
  void initState() {
    super.initState();

    _futureFun = _initData();

    // listen to change of interface language
    _notifierLocale.stream.listen((Locale? locale) async {
      if (locale != null &&
          (locale.languageCode != _currentLocale.languageCode)) {
        final prefs = await SharedPreferences.getInstance();
        String newLocale = locale.languageCode;
        await prefs.setString('appLocale', newLocale);
        setState(() {
          _currentLocale = locale;
        });
      }
    });
  }

  @override
  void dispose() {
    _notifierLocale.close();
    super.dispose();
  }

  Future<bool>? _initData() async {
    final prefs = await SharedPreferences.getInstance();
    String? lastAppLocaleCode = prefs.getString('appLocale');
    if (lastAppLocaleCode != null) {
      _currentLocale = Locale(lastAppLocaleCode, '');
    }
    return true;
  }

  List<Locale> getSupportedLocales() {
    List<Locale> locales = [];
    for (var element in AppConfig.supportedLanguage) {
      locales.add(Locale(element, ''));
    }
    return locales;
  }

  final _auth = FirebaseAuth.instance;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _futureFun,
      builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
        if (snapshot.hasData) {
          return GestureDetector(
            //this is for hide keyborad
            onTap: () {
              FocusScopeNode currentFocus = FocusScope.of(context);

              if (!currentFocus.hasPrimaryFocus) {
                currentFocus.unfocus();
              }
            },
            child: MaterialApp(
              debugShowCheckedModeBanner: false,
              initialRoute: _auth.currentUser != null
                  // ? SigninScreen.screenRoute
                  // : SigninScreen.screenRoute,

                  // ? ChatScreen.screenRoute
                  // ? WelcomeScreen.screenRoute
                  // : WelcomeScreen.screenRoute,
                  ? HomeScreen.screenRoute
                  : HomeScreen.screenRoute,
              //  ? NotifactionServer.screenRoute
              //  : NotifactionServer.screenRoute,
              routes: <String, WidgetBuilder>{
                HomeScreen.screenRoute: (BuildContext context) =>
                    HomeScreen(notifierLocale: _notifierLocale),
                //HomeScreen.screenRoute: (context) => HomeScreen(),
                WelcomeScreen.screenRoute: (context) => WelcomeScreen(),
                SigninScreen.screenRoute: (context) => SigninScreen(),
                RegistrationScreen.screenRoute: (context) =>
                    RegistrationScreen(),
                ChatScreen.screenRoute: (context) => ChatScreen(),
                BoltScreen.screenRoute: (context) => BoltScreen(),
                NotifactionServer.screenRoute: (context) => NotifactionServer(),
                NotifactionServer.screenRoute: (context) => NotifactionServer(),
                NotifactionScreen.screenRoute: (context) => NotifactionScreen(),
              },
              title: _currentLocale.languageCode == "ar"
                  ? "إسم التطبيق"
                  : 'App Name',
              locale: _currentLocale,
              localizationsDelegates: const [
                TranslationService.delegate,
                GlobalMaterialLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate,
                GlobalCupertinoLocalizations.delegate,
              ],
              supportedLocales: getSupportedLocales(),
            ),
          );
        } else if (snapshot.hasError) {
          return Container(); // here return your ErrorScreen widget
        } else {
          return Container(); // here return your SplashScreen widget
        }
      },
    );
  }
}
// class MyApp extends StatelessWidget {



//   final _auth = FirebaseAuth.instance;

//   @override
//   Widget build(BuildContext context) {
//     return GestureDetector(
//         //this is for hide keyborad
//         onTap: () {
//           FocusScopeNode currentFocus = FocusScope.of(context);

//           if (!currentFocus.hasPrimaryFocus) {
//             currentFocus.unfocus();
//           }
//         },
//         child: MaterialApp(
//           debugShowCheckedModeBanner: false,
//           initialRoute: _auth.currentUser != null
//               // ? ChatScreen.screenRoute
//               // ? WelcomeScreen.screenRoute
//               // : WelcomeScreen.screenRoute,
//               ? HomeScreen.screenRoute
//               : HomeScreen.screenRoute,
//           routes: {
//             HomeScreen.screenRoute: (context) => HomeScreen(),
//             WelcomeScreen.screenRoute: (context) => WelcomeScreen(),
//             SigninScreen.screenRoute: (context) => SigninScreen(),
//             RegistrationScreen.screenRoute: (context) => RegistrationScreen(),
//             ChatScreen.screenRoute: (context) => ChatScreen(),
//             BoltScreen.screenRoute: (context) => BoltScreen(),
//           },
//         ));
//   }
// }
