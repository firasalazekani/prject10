class Data {
  final int id;
  final String imagePath;
  final String name;

  Data({
    required this.id,
    required this.imagePath,
    required this.name,
  });
}

List<Data> navBtn = [
  Data(id: 1, imagePath: 'images/search.png', name: 'Search'),
  Data(id: 0, imagePath: 'images/filter2.png', name: 'Filter'),
  Data(id: 2, imagePath: 'images/question.png', name: 'How To'),
  Data(id: 3, imagePath: 'images/notification.png', name: 'Notification'),
  Data(id: 4, imagePath: 'images/chat2.png', name: 'Chat'),
];
