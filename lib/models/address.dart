import 'dart:convert';

List<Address> addressFromJson(String str) =>
    List<Address>.from(json.decode(str).map((x) => Address.fromJson(x)));

String addressToJson(List<Address> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Address {
  Address({
    required this.place,
    required this.city,
    required this.country,
    required this.latitude,
    required this.longitude,
  });

  String place;
  String city;
  String country;
  String latitude;
  String longitude;

  factory Address.fromJson(Map<String, dynamic> json) => Address(
        place: json["place"],
        city: json["city"],
        country: json["country"],
        latitude: json["latitude"],
        longitude: json["longitude"],
      );

  Map<String, dynamic> toJson() => {
        "place": place,
        "city": city,
        "country": country,
        "latitude": latitude,
        "longitude": longitude,
      };
}
