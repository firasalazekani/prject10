import 'dart:async';
import 'dart:io';
import 'dart:ui';
import 'dart:convert';

import 'dart:math' as math;
import 'dart:developer';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:badges/badges.dart';
import 'package:clustr_map_test_1/Screens/chat/bolt_screen.dart';
import 'package:clustr_map_test_1/Screens/chat/signin_screen.dart';
import 'package:clustr_map_test_1/Screens/chat/welcome_screen.dart';
import 'package:clustr_map_test_1/Screens/notifaction/notifaction_screen.dart';
import 'package:clustr_map_test_1/Screens/notifaction/notifaction_server.dart';
import 'package:clustr_map_test_1/models/address.dart';
import 'package:clustr_map_test_1/servies/local/translation_service.dart';
import 'package:clustr_map_test_1/servies/location_services.dart';
import 'package:clustr_map_test_1/widgets/loading.dart';
import 'package:clustr_map_test_1/widgets/loading_alert.dart';
import 'package:clustr_map_test_1/widgets/local/config/app_config.dart';
import 'package:firebase_core/firebase_core.dart';
//import 'package:fluster/fluster.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:location/location.dart';
import 'package:rxdart/rxdart.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../helpers/map_helper.dart';
import '../helpers/map_marker.dart';
//import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:loading_alert_dialog/loading_alert_dialog.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:delayed_display/delayed_display.dart';
//import 'package:custom_info_window/custom_info_window.dart';
import 'package:clippy_flutter/triangle.dart';

import '../navigator/constants/color.dart';
import '../navigator/constants/text_style.dart';
import '../navigator/models/data.dart';
import '../navigator/widgets/custom_paint.dart';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map_marker_cluster/flutter_map_marker_cluster.dart';
// //import 'package:latlong/latlong.dart';
import 'package:latlong2/latlong.dart' as latLng;

//badge=true
// في الاشعارات من اجل ان يظهر رقم بجنب الجرس تبع عدد الاشعارات غير المقروؤة
final _firestor = FirebaseFirestore.instance;
late User signedInUser;
late User signedInUser1;
final _auth = FirebaseAuth.instance;
bool isMount = true;
Future<List<Address>> fetchAddress() async {
  final reponse = await http.get(Uri.parse(
      'https://run.mocky.io/v3/f92d4c37-4f7f-4ec9-994a-f096bf3bcd9c'));
  List<Address> addresslist = [];

  if (reponse.statusCode == 200) {
    var userjson = json.decode(reponse.body);
    for (var josnData in userjson) {
      addresslist.add(Address.fromJson(josnData));
    }
    return addresslist;
  } else {
    return throw Exception('Failed to load album');
  }
}

//final user = _auth.currentUser;

void getCurrentUser() {
  try {
    final user = _auth.currentUser;
    if (user != null) {
      signedInUser = user;
      signedInUser1 = user;
      // LogUser = true;
      print('signedInUser ${signedInUser.email}');
      print('signedInUser1 ${signedInUser1.email}');
    } else {
      //  Navigator.pushNamed(context, WelcomeScreen.screenRoute);
    }
  } catch (e) {
    print('e :$e');
  }
}

class HomeScreen extends StatefulWidget {
  static const String screenRoute = 'home_screen';

  final BehaviorSubject<Locale?> notifierLocale;

  const HomeScreen({Key? key, required this.notifierLocale}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final PopupController _popupController = PopupController();
  final Set<Marker> markers = Set();
  // this varible for Josn
  late Future<List<Address>> futureAddress;
  final List<Address> _address = [];
  //----------//
  final List<latLng.LatLng> _markerLocations = [];

  int selectBtn = 0;

  final storage = new FlutterSecureStorage();

//this variable for instance to get toke
  var fbm = FirebaseMessaging.instance;

  void getPostionformjson() {
    for (var address in _address) {
      double s1 = double.parse(address.latitude);
      double s2 = double.parse(address.longitude);

      _markerLocations.add(latLng.LatLng(s1, s2));
    }
  }

  Widget WindowInfoFiras(latLng.LatLng vs) {
    for (var address in _address) {
      double s1 = double.parse(address.latitude);
      double s2 = double.parse(address.longitude);
      latLng.LatLng addressLatLang = latLng.LatLng(s1, s2);
      if (addressLatLang == vs) {
        return Column(
          children: [
            Expanded(
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.blue,
                  borderRadius: BorderRadius.circular(25),
                ),
                width: double.infinity,
                height: double.infinity,
                child: Padding(
                  padding: const EdgeInsets.all(3.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        '${address.place}',
                        style: TextStyle(fontSize: 15, color: Colors.white),
                      ),
                      Text(
                        '${address.city} , ${address.country}',
                        style: TextStyle(fontSize: 13, color: Colors.white),
                      ),
                      Text(
                        'latitude =$s1 , longitude=$s2',
                        style: TextStyle(fontSize: 13, color: Colors.white),
                      ),
                    ],
                  ),
                ),
                // width: 200,
                // height: 100,
              ),
            ),
            Triangle.isosceles(
              edge: Edge.BOTTOM,
              child: Container(
                color: Colors.blue,
                width: 20.0,
                height: 10.0,
              ),
            ),
          ],
        );
      }
    }
    return Text('data');
  }

  void createMarker() {
    if (_markerLocations.length != 0) {
      setState(() {
        _isMapLoading = false;
      });
      for (var address in _address) {
        double s1 = double.parse(address.latitude);
        double s2 = double.parse(address.longitude);
        markers.add(
          Marker(
            anchorPos: AnchorPos.align(AnchorAlign.center),
            height: 30,
            width: 30,
            point: latLng.LatLng(s1, s2),
            builder: (ctx) {
              return Icon(Icons.pin_drop);
            },
          ),
        );
        // print(
        //     'position : ${latLng.LatLng(s1, s2)}, title: ${address.place}, snippet: ${address.city} , ${address.country} ');
      }
    }
  }

  //يعمل فقط في حالة التيرمنل
  initalMessages() async {
    var terminalMessage = await FirebaseMessaging.instance.getInitialMessage();
    if (terminalMessage != null) {
      print('=========== SigninScreen.screenRoute==============');
      // Navigator.pushNamed(context, SigninScreen.screenRoute);
      // Navigator.pushNamed(context, NotifactionScreen.screenRoute);
    }
  }

  // // var serverToken = " AIzaSyCYK3r5wwVd_pWlkyxFzolduWXA9lL3a_E";
  // var serverToken =
  //     "AAAAWmOe574:APA91bHVBxATiKWpxuQXm8_bc8Ub3EHQQ9DoBgsihtnNR7LkKfUh7VcWxzoMA1t8xemv93-R5lMpNXLbRbVxQR8O8vLpbGtAw9CUdGHrFq02zOG7DpctiFInVLGm1vEyyuYyA4Y3P2XS";

  // sendNotfiy(String title, String body, String id) async {
  //   await http.post(Uri.parse('https://fcm.googleapis.com/fcm/send'),
  //       headers: <String, String>{
  //         'content-Type': 'application/json',
  //         'Authorization': 'key$serverToken',
  //       },
  //       body: jsonEncode(<String, dynamic>{
  //         'notification': <String, dynamic>{
  //           'body': body.toString(),
  //           'title': title.toString()
  //         },
  //         'priority': 'high',
  //         'data': <String, dynamic>{
  //           'click_action': 'FLUTTER_NOTIFICATION_CLICK',
  //           'id': id.toString(),
  //           "name": "Firas",
  //           "lastname": "Alzekani",
  //         },
  //         //التوكين الذي اريد ارسال هذه النوتيفكيشن اليه
  //         //وفي هذه الحال انا سوف ارسل الى جهازي لانو ما عندي توكن ارسل اليه
  //         'to': await FirebaseMessaging.instance.getToken()
  //       }));
  // }

  getMessage() {
    FirebaseMessaging.onMessage.listen((event) {
      print("==========================");
      print(event.notification?.title);
      print(event.notification?.body);
      print(event.data["name"]);
      print(event.data["lastname"]);
    });
  }

//خاصة بالانظمة لابل
  requestPermssion() async {
    FirebaseMessaging messaging = FirebaseMessaging.instance;

    NotificationSettings settings = await messaging.requestPermission(
      alert: true,
      announcement: false,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: false,
      sound: true,
    );

    if (settings.authorizationStatus == AuthorizationStatus.authorized) {
      print('User granted permission');
    } else if (settings.authorizationStatus ==
        AuthorizationStatus.provisional) {
      print('User granted provisional permission');
    } else {
      print('User declined or has not accepted permission');
    }
  }

  Future backgroundMessage(RemoteMessage masseging) async {
    print("========== backgroundMessage===========");
    print("${masseging.notification?.body}");
  }

  //هذا الميثود يتم اضافته في اعلى الشجرة اي في المين
  //main
  //ويكون في حالة ال
  //background and tremial
//الفائدة من هذا الميثود اذا بدي اعمل اي شيء على داتا بيز
  // FirebaseMessaging.onBackgroundMessage((backgroundMessage));

  int CounterNotification = 0;
  @override
  void initState() {
    //super.initState();
    getCurrentUser();
    getMessage();
    initalMessages();
    requestPermssion();
    //work in programm in background and termail
    //هاد الميثود لا يظهر اشعار عندما يكون التطبيق مفتوح وانما يعمل ويرسل
    //اشعار عندما يكون التطبيق في الخلفية او مغلق
    fbm.getToken().then((value) {
      print('---------------- Token -----------');
      print(value);

      print('---------------- End Token -----------');
    });
    //عندما يكون التطبيق مفتوح واريد ان احصل على اشعار
    //للحصول على معلومات بداخل الرسالة من تايتل وبدي
    //وعند الارسال من فيربيز يتم الطباعة مباشرتا
    FirebaseMessaging.onMessage.listen((event) {
      print('=========== Data Notification==============');
      print('${event.notification!.body}');
      print('===========================================');
      if (this.mounted) {
        setState(() {
          CounterNotification++;
          print('CounterNotification = $CounterNotification');
        });
      }
      AwesomeDialog(
          context: context,
          dialogType: DialogType.INFO,
          title: "title",
          // transitionAnimationDuration: const Duration(milliseconds: 300),
          autoHide: const Duration(milliseconds: 3000),
          body: Text("${event.notification!.body}"))
        ..show();
      //ممكن بدلا من اظهار الرسالة وجهه لصفحة تانية
      // Navigator.pushNamed(context, WelcomeScreen.screenRoute);

      //onMessageOpenedApp
      //يعمل في حال كان التطبيق  باك كراوند فقطططططططط عند ارسال رسالة
      //من الفير بيز في خلفية سوف يرسل اشعار وعند النقر عليه سوف يفتح
      //صفحة ويلكمسكرين
      FirebaseMessaging.onMessageOpenedApp.listen((event) {
        // setState(() {
        //   CounterNotification++;
        //   print('CounterNotification = $CounterNotification');
        // });
        print('=========== WelcomeScreen.screenRoute==============');
        //Navigator.pushNamed(context, WelcomeScreen.screenRoute);
        Navigator.pushNamed(context, NotifactionScreen.screenRoute);
        FirebaseMessaging.onBackgroundMessage((backgroundMessage));
      });
    });
    fetchAddress().then(
      (value) {
        setState(() {
          _address.addAll(value);
          getPostionformjson();
          createMarker();
          // _getFirstMyLocation();
          // _initMarkers();
        });
      },
    );
    futureAddress = fetchAddress();
    super.initState();
  }

  // void dispose() {
  //   // TODO: implement dispose
  //   setState(() {
  //     this.mounted;
  //   });
  //   // code omitted..        super.dispose();
  // }

  // late bool ditrbutedMarker = false;
  // late LatLng? currentPostionWithoutMove;
  // Future<void> _getFirstMyLocation() async {
  //   LocationData _Location = await LocationService().getLocation();
  //   //  if (ditrbutedMarker == false) {
  //   _Gotogo(LatLng(_Location.latitude!, _Location.longitude!));
  //   print(
  //       "First Location: ${_Location.latitude!}, long: ${_Location.longitude!}");
  //   ditrbutedMarker = true;
  //   // } else {
  //   _Gotogo(LatLng(33.5292273, 36.2783447));
  //   //  }
  //   createMarkers();
  // }

  // Future<void> _Gotogo(LatLng _location) async {
  //   currentPostionWithoutMove = _location;
  // }

  // var markerbitmap;

  // createMarkers() async {
  //   markerbitmap = await BitmapDescriptor.fromAssetImage(
  //     ImageConfiguration(),
  //     'images/location_icon3.png',
  //   );
  //   _markers.add(Marker(
  //     //add start location marker
  //     markerId: MarkerId(currentPostionWithoutMove.toString()),
  //     position: currentPostionWithoutMove!,
  //     //rotation: -45, //position of marker
  //     infoWindow: InfoWindow(
  //       //popup info
  //       title: 'You Here ',
  //       snippet: 'Start Marker',
  //     ),
  //     icon: markerbitmap, //Icon for Marker
  //   ));
  //   setState(() {});
  // }

  // final Completer<GoogleMapController> _mapController = Completer();
  // CustomInfoWindowController _customInfoWindowController =
  //     CustomInfoWindowController();

  // void dispose() {
  //   _customInfoWindowController.dispose();
  //   //Connectivity().onConnectivityChanged.cancel();
  //   // if (mounted) {
  //   //   setState(() => _HomeScreenState);
  //   // }

  //   super.dispose();
  // }

//   /// Set of displayed markers and cluster markers on the map
//   final Set<Marker> _markers = Set();

//   /// Minimum zoom at which the markers will cluster
//   final int _minClusterZoom = 0;

//   /// Maximum zoom at which the markers will cluster
//   final int _maxClusterZoom = 19;

//   /// [Fluster] instance used to manage the clusters
//   Fluster<MapMarker>? _clusterManager;

//   /// Current map zoom. Initial zoom will be 15, street level
//   // double _currentZoom = 15;
//   double _currentZoom = 6.2000;

//   /// Map loading flag
  bool _isMapLoading = true;

//   /// Markers loading flag
//   bool _areMarkersLoading = true;
//   bool _FirstareMarkersLoading = true;

//   /// Url image used on normal markers
//   final String _markerImageUrl =
//       'https://img.icons8.com/office/80/000000/marker.png';

//   /// Color of the cluster circle
//   final Color _clusterColor = Colors.blue;

//   /// Color of the cluster text
//   final Color _clusterTextColor = Colors.white;

//   final List<LatLng> _markerLocations = [];

//   void getPostionformjson() {
//     for (var address in _address) {
//       double s1 = double.parse(address.latitude);
//       double s2 = double.parse(address.longitude);

//       _markerLocations.add(LatLng(s1, s2));
//     }
//   }

//   /// Called when the Google Map widget is created. Updates the map loading state
//   /// and inits the markers.
//   void _onMapCreated(GoogleMapController controller) {
//     _mapController.complete(controller);
//     setState(() {
//       _isMapLoading = false;
//     });

//     _initMarkers();
//   }

//   /// Inits [Fluster] and all the markers with network images and updates the loading state.
//   void _initMarkers() async {
//     final List<MapMarker> markers = [];
// //_markerLocations
//     if (_markerLocations.length != 0) {
//       // for (LatLng markerLocation in _markerLocations) {
//       for (var address in _address) {
//         final BitmapDescriptor markerImage =
//             await MapHelper.getMarkerImageFromUrl(_markerImageUrl);
//         double s1 = double.parse(address.latitude);
//         double s2 = double.parse(address.longitude);
//         markers.add(
//           MapMarker(
//               id: _address.indexOf(address).toString(),
//               position: LatLng(s1, s2),
//               icon: markerImage,
//               onTop: () {
//                 _customInfoWindowController.addInfoWindow!(
//                     Column(
//                       children: [
//                         Expanded(
//                           child: Container(
//                             decoration: BoxDecoration(
//                               color: Colors.blue,
//                               borderRadius: BorderRadius.circular(25),
//                             ),
//                             child: Padding(
//                               padding: const EdgeInsets.all(3.0),
//                               child: Column(
//                                 mainAxisAlignment: MainAxisAlignment.center,
//                                 children: [
//                                   Text(
//                                     '${address.place}',
//                                     style: TextStyle(
//                                         fontSize: 15, color: Colors.white),
//                                   ),
//                                   Text(
//                                     '${address.city} , ${address.country}',
//                                     style: TextStyle(
//                                         fontSize: 13, color: Colors.white),
//                                   ),
//                                 ],
//                               ),
//                             ),
//                             width: double.infinity,
//                             height: double.infinity,
//                           ),
//                         ),
//                         Triangle.isosceles(
//                           edge: Edge.BOTTOM,
//                           child: Container(
//                             color: Colors.blue,
//                             width: 20.0,
//                             height: 10.0,
//                           ),
//                         ),
//                       ],
//                     ),
//                     LatLng(s1, s2));
//               }),
//         );

//         print(
//             'position : ${LatLng(s1, s2)}, title: ${address.place}, snippet: ${address.city} , ${address.country} ');
//       }
//       final String title = "";
//       final String spin = "";
//       _clusterManager = await MapHelper.initClusterManager(
//         markers,
//         _minClusterZoom,
//         _maxClusterZoom,
//         title,
//         spin,
//       );

//       await _updateMarkers();
//     } else {
//       print('list marker length is false');
//     }
//   }

  // double? getZoomLevel(double radius) {
  //   double? zoomLevel = 11;
  //   if (radius > 0) {
  //     double radiusElevated = radius + radius / 2;
  //     double scale = radiusElevated / 500;
  //     zoomLevel = 16 - math.log(scale) / math.log(20);
  //   }
  //   zoomLevel = num.parse(zoomLevel.toStringAsFixed(20)) as double?;
  //   return zoomLevel;
  // }

  // /// Gets the markers and clusters to be displayed on the map for the current zoom level and
  // /// updates state.
  // Future<void> _updateMarkers([double? updatedZoom]) async {
  //   if (_clusterManager == null || updatedZoom == _currentZoom) return;

  //   if (updatedZoom != null) {
  //     print("updated zoom = ${updatedZoom} ,  currentZom = ${_currentZoom}");

  //     //_currentZoom = updatedZoom + 19;
  //     _currentZoom = getZoomLevel(updatedZoom)!;
  //     print("=====================================");
  //     print("  currentZom = ${_currentZoom}");
  //     print("=====================================");
  //   }

  //   setState(() {
  //     _areMarkersLoading = true;
  //     _FirstareMarkersLoading = false;
  //   });

  //   final updatedMarkers = await MapHelper.getClusterMarkers(
  //     _clusterManager,
  //     _currentZoom,
  //     _clusterColor,
  //     _clusterTextColor,
  //     80,
  //   );

  //   _markers
  //     ..clear()
  //     ..addAll(updatedMarkers);
  //   _getFirstMyLocation();

  //   setState(() {
  //     _areMarkersLoading = false;
  //     _FirstareMarkersLoading = false;
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          //  Opacity(
          //    opacity: _isMapLoading ? 0 : 1,
          // child:
          //  Stack(
          //   children: <Widget>[
          FlutterMap(
            options: MapOptions(
              // center: points[0],
              //نقطة البداية من اجل الوقف فوق خرية سوريا
              center: latLng.LatLng(35.163529, 39.348444),
              // zoom: 6,
              //زوم البداية فوق الخريطة
              zoom: 6.0900,

              plugins: [
                MarkerClusterPlugin(),
              ],
              //onTap: (_) => _popupController.hideAllPopups(),
              onTap: (_, __) => _popupController.hideAllPopups(),
            ),
            layers: [
              TileLayerOptions(
                urlTemplate:
                    'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                subdomains: ['a', 'b', 'c'],
              ),
              MarkerClusterLayerOptions(
                  //maxClusterRadius: 120,
                  maxClusterRadius: 160,
                  // disableClusteringAtZoom: 6,
                  //من اجل زوم على الكلستر مثلا يطلع حمص فيها 4 حماة فيها 6
                  //اذا حطيته مثلا 6 ما بيطلع عندي كلستر لحماة وحمص
                  disableClusteringAtZoom: 15,
                  size: Size(40, 40),
                  anchor: AnchorPos.align(AnchorAlign.center),
                  fitBoundsOptions: FitBoundsOptions(
                    padding: EdgeInsets.all(50),
                  ),
                  markers: markers.toList(),
                  polygonOptions: PolygonOptions(
                      borderColor: Colors.blueAccent,
                      color: Colors.black12,
                      borderStrokeWidth: 3),
                  popupOptions: PopupOptions(
                    popupSnap: PopupSnap.markerTop,
                    popupController: _popupController,
                    popupBuilder: (_, marker) => Container(
                      decoration: BoxDecoration(
                        color: Colors.blue[1000],
                        borderRadius: BorderRadius.circular(25),
                      ),
                      width: 200,
                      height: 120,
                      child: GestureDetector(
                        onTap: () => debugPrint("Popup tap! ${marker.point}"),
                        child: WindowInfoFiras(marker.point),
                      ),
                    ),
                  ),
                  builder: (context, markers) {
                    return FloatingActionButton(
                      onPressed: null,
                      child: Text(markers.length.toString()),
                    );
                  }),
            ],
          ),
          Padding(
            padding: EdgeInsets.only(
                //top: 650,
                top: MediaQuery.of(context).size.height * 0.65,
                left: 10,
                right: 20,
                bottom: 10),
            child: Align(
              alignment: Alignment.centerRight,
              child: DropdownButton(
                dropdownColor: Colors.grey[200],
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                  backgroundColor: Colors.grey[200],
                ),
                onChanged: (v) async {
                  widget.notifierLocale.add(Locale(v.toString(), ""));
                  //SharedPreferences
                  //مهمة هذا المتحول تخزين قيمة نستطيع استخدامه في اي واجهة بالتطبيق كامل
                  // final prefs = await SharedPreferences.getInstance();
                  // prefs.setString('lang', v.toString());
                  await storage.write(key: 'jwt', value: v.toString());
                },
                value: TranslationService.currentLocale(context),
                items: List.generate(
                    AppConfig.supportedLanguage.length,
                    (index) => DropdownMenuItem(
                        value: AppConfig.supportedLanguage[index],
                        child: Text(AppConfig.supportedLanguage[index]))),
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: navigationBar(),
          ),
          // showspinefalse(),
          //   ],
          // ),
          // ),
          // ),
          //  Opacity(
          //   opacity: _isMapLoading ? 1 : 0,
          //  child:
          if (_isMapLoading == true)
            Container(
                width: MediaQuery.of(context).size.width,
                color: Color.fromARGB(255, 41, 181, 216).withOpacity(0.3),
                child: AlertDialog(
                  content: new Row(
                    children: [
                      CircularProgressIndicator(),
                      Container(
                          margin: EdgeInsets.only(left: 7),
                          //   child: Text("Loading...")),
                          child: Text(TranslationService.of(context)!
                              .translate('Loading...'))),
                    ],
                  ),
                )),
          //  ),
        ],
      ),
    );
  }

  AnimatedContainer navigationBar() {
    return AnimatedContainer(
      height: 70.0,
      duration: const Duration(milliseconds: 400),
      decoration: BoxDecoration(
        color: white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(selectBtn == 0 ? 0.0 : 20.0),
          topRight:
              Radius.circular(selectBtn == navBtn.length - 1 ? 0.0 : 20.0),
        ),
      ),
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            for (int i = 0; i < navBtn.length; i++)
              GestureDetector(
                // onTap: () => setState(() => selectBtn = i),

                onTap: () {
                  setState(() => selectBtn = i);
                  if (i == 3) {
                    //if (signedInUser1.email != null) {
                    // _auth.currentUser

                    if (_auth.currentUser != null) {
                      if (signedInUser.email == 'firas@test.com') {
                        print('User From Back : ${signedInUser1.email}');
                        // Navigator.popAndPushNamed(
                        Navigator.popAndPushNamed(
                            context, NotifactionServer.screenRoute);
                      } else {
                        // Navigator.popAndPushNamed(
                        CounterNotification = 0;
                        Navigator.popAndPushNamed(
                            context, NotifactionScreen.screenRoute);
                      }
                    } else {
                      Navigator.pushNamed(context, WelcomeScreen.screenRoute);
                    }
                    //   //  Navigator.pushNamed(context, NotifactionServer.screenRoute);
                    // }
                    // else {
                    //   Navigator.pop(context, HomeScreen.screenRoute);
                  }
                  if (i == 4) {
                    // if (i == 4) {
                    if (_auth.currentUser != null) {
                      Navigator.popAndPushNamed(
                          context, BoltScreen.screenRoute);
                    } else {
                      Navigator.popAndPushNamed(
                          context, WelcomeScreen.screenRoute);
                    }
                  }
                  //  else {
                  //   Navigator.pop(context, HomeScreen.screenRoute);
                  // }
                  //}
                },
                child: iconBtn(i),
              ),
          ],
        ),
      ),
    );
  }

  SizedBox iconBtn(int i) {
    bool isActive = selectBtn == i ? true : false;
    var height = isActive ? 60.0 : 0.0;
    var width = isActive ? 50.0 : 0.0;
    return SizedBox(
      width: 75.0,
      child: Stack(
        children: [
          Align(
            alignment: Alignment.topCenter,
            child: AnimatedContainer(
              height: height,
              width: width,
              duration: const Duration(milliseconds: 600),
              child: isActive
                  ? CustomPaint(
                      painter: ButtonNotch(),
                    )
                  : const SizedBox(),
            ),
          ),
          Align(
            alignment: Alignment.center,
            child: Image.asset(
              navBtn[i].imagePath,
              color: isActive ? selectColor : black,
              scale: 2,
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Text(
              //  navBtn[i].name,
              TranslationService.of(context)!.translate('${navBtn[i].name}'),
              style: isActive ? bntText.copyWith(color: selectColor) : bntText,
            ),
          ),
          if (CounterNotification != 0)
            if (i == 3)
              //For Number Notfication
              Badge(
                position: BadgePosition.custom(top: 10, end: -55),
                // position: BadgePosition.bottomEnd(),
                badgeAnimation: BadgeAnimation.fade(),
                badgeContent: Text('$CounterNotification'),
                child: Text(''),
              ),
        ],
      ),
    );
  }
}

//),
// Opacity(
//   opacity: _isMapLoading ? 1 : 0,
//   child: Center(child: CircularProgressIndicator()),
// ),

//// Map markers loading indicator

// if (_areMarkersLoading)
//   Padding(
//     padding: const EdgeInsets.all(0.0),
//     child: _FirstareMarkersLoading
//         ? Container(
//             width: MediaQuery.of(context).size.width,
//             color: Color.fromARGB(255, 41, 181, 216).withOpacity(0.3),
//             child: AlertDialog(
//               content: new Row(
//                 children: [
//                   CircularProgressIndicator(),
//                   Container(
//                       margin: EdgeInsets.only(left: 7),
//                       //   child: Text("Loading...")),
//                       child: Text(TranslationService.of(context)!
//                           .translate('Loading...'))),
//                 ],
//               ),
//             ),
//           )
//         : Align(
//             alignment: Alignment.topCenter,
//             child: Card(
//               elevation: 2,
//               color: Colors.grey.withOpacity(0.9),
//               child: Padding(
//                 padding: const EdgeInsets.all(4),
//                 // child: Text(
//                 //   'Loading...',
//                 child: Text(
//                   TranslationService.of(context)!
//                       .translate('Loading...'),
//                   style: TextStyle(color: Colors.white),
//                 ),
//               ),
//             ),
//           ),
//   ),

// Padding(
//     padding: EdgeInsets.only(
//         //top: 650,
//         top: MediaQuery.of(context).size.height * 0.25,
//         left: 10,
//         right: 20,
//         bottom: 10),
//     child: Align(
//       alignment: Alignment.centerRight,
//       child: MaterialButton(
//         onPressed: () async {
//           // await sendNotfiy("title", "body", "1");
//           if (user != null) {
//             Navigator.popAndPushNamed(
//                 context, NotifactionServer.screenRoute);
//           } else {
//             Navigator.popAndPushNamed(
//                 context, WelcomeScreen.screenRoute);
//           }
//           //  Navigator.pushNamed(context, NotifactionServer.screenRoute);
//         },
//         child: Text("send Notify"),
//       ),
//     )),
