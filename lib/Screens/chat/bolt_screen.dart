import 'package:clustr_map_test_1/Screens/home_screen.dart';
import 'package:clustr_map_test_1/servies/local/translation_service.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

import '../chat/chat_screen.dart';
import 'package:flutter/material.dart';

class BoltScreen extends StatefulWidget {
  static const String screenRoute = 'bolt_screen';

  const BoltScreen({super.key});

  @override
  State<BoltScreen> createState() => _BoltScreenState();
}

class _BoltScreenState extends State<BoltScreen> {
  final _auth = FirebaseAuth.instance;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        //backgroundColor: Colors.blue[800]!,
        backgroundColor: Colors.blue,
        // title: Text('Chatbot')),
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            // Image.asset('images/logo1.png', height: 25),
            // SizedBox(width: 20),
            Text('Chatbot'),

            // Alignment(x, y)
          ],
        ),
        actions: [
          IconButton(
              onPressed: () {
                _auth.signOut();
                //من اجل وقت يعمل تسجيل خروج يعمل الغاء كي لا تصله الاشعارات
                //وقت يعمل تسجيل دخول بنرد بنفعل هذه الميزة
                FirebaseMessaging.instance.unsubscribeFromTopic("notification");

                Navigator.pushNamed(context, HomeScreen.screenRoute);
              },
              icon: Icon(Icons.close)),
          IconButton(
              onPressed: () {
                //_auth.signOut();
                //من اجل وقت يعمل تسجيل خروج يعمل الغاء كي لا تصله الاشعارات
                //وقت يعمل تسجيل دخول بنرد بنفعل هذه الميزة
                // FirebaseMessaging.instance.unsubscribeFromTopic("notification");

                Navigator.popAndPushNamed(context, HomeScreen.screenRoute);
              },
              icon: Icon(Icons.home)),
        ],
        // leading: IconButton(
        //     onPressed: () {
        //       Navigator.pop(context);
        //     },
        //     icon: Icon(Icons.arrow_back)),
      ),
      body: SafeArea(
          child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CircleAvatar(
              radius: 80.0,
              backgroundImage: AssetImage('images/Robot.png'),
              backgroundColor: Colors.white,
            ),
            SizedBox(
              height: 15.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                // SizedBox(
                //   width: 40.0,
                // ),
                Text(
                  TranslationService.of(context)!.translate("Hey! I am"),
                  style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  width: 10.0,
                ),
                Text(
                  'bolt',
                  style: TextStyle(color: Colors.blue, fontSize: 20.0),
                ),
              ],
            ),
            SizedBox(
              height: 12.0,
            ),
            Text(
              TranslationService.of(context)!
                  .translate("I am here to help answer your questions"),
              style: TextStyle(
                fontSize: 18.0,
              ),
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 15.0,
            ),
            Text(
              TranslationService.of(context)!.translate("Press to start chat"),
              style: TextStyle(
                fontSize: 20.0,
              ),
            ),
            SizedBox(
              height: 15.0,
            ),
            FloatingActionButton(
              onPressed: () {
                // Add your onPressed code here!
                Navigator.pushNamed(context, ChatScreen.screenRoute);
              },
              backgroundColor: Colors.blue,
              //  child: const Icon(Icons.arrow_forward),
              child: Container(
                // color: Colors.orange[800],
                // color: Colors.blue[800],
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Color.fromARGB(255, 100, 224, 255),
                      Color.fromARGB(255, 31, 148, 243)
                    ],
                    begin: Alignment.topRight,
                    end: Alignment.bottomCenter,
                  ),
                  borderRadius: BorderRadius.circular(30.0),
                ),
                height: 270,
                alignment: Alignment.center,
                child: const Icon(Icons.arrow_forward),
              ),
            ),
          ],
        ),
      )),
    );
  }
}

//  