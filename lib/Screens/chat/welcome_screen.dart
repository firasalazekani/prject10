import 'package:clustr_map_test_1/Screens/home_screen.dart';
import 'package:clustr_map_test_1/servies/local/translation_service.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

import '../chat/Signin_Screen.dart';
import '../chat/registration_Screen.dart';
import 'package:flutter/material.dart';
import 'package:clustr_map_test_1/widgets/chat//my_button.dart';

class WelcomeScreen extends StatefulWidget {
  static const String screenRoute = 'welcome_screen';
  @override
  State<WelcomeScreen> createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  @override
  final _auth = FirebaseAuth.instance;

  Widget build(BuildContext context) {
    // return Scaffold(
    //   backgroundColor: Colors.white,
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        // backgroundColor: Colors.blue[800]!,
        backgroundColor: Colors.blue,

        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            // Image.asset('images/logo1.png', height: 25),
            // SizedBox(width: 20),
            Text('Welcome Screen'),

            // Alignment(x, y)
          ],
        ),
        actions: [
          IconButton(
              onPressed: () {
                _auth.signOut();
                //من اجل وقت يعمل تسجيل خروج يعمل الغاء كي لا تصله الاشعارات
                //وقت يعمل تسجيل دخول بنرد بنفعل هذه الميزة
                FirebaseMessaging.instance.unsubscribeFromTopic("notification");

                Navigator.pushNamed(context, HomeScreen.screenRoute);
              },
              icon: Icon(Icons.close)),
          IconButton(
              onPressed: () {
                //_auth.signOut();
                //من اجل وقت يعمل تسجيل خروج يعمل الغاء كي لا تصله الاشعارات
                //وقت يعمل تسجيل دخول بنرد بنفعل هذه الميزة
                // FirebaseMessaging.instance.unsubscribeFromTopic("notification");

                Navigator.popAndPushNamed(context, HomeScreen.screenRoute);
              },
              icon: Icon(Icons.home)),
        ],
        leading: IconButton(
            onPressed: () {
              //_auth.signOut();
              //من اجل وقت يعمل تسجيل خروج يعمل الغاء كي لا تصله الاشعارات
              //وقت يعمل تسجيل دخول بنرد بنفعل هذه الميزة
              // FirebaseMessaging.instance.unsubscribeFromTopic("notification");

              Navigator.popAndPushNamed(context, HomeScreen.screenRoute);
            },
            icon: Icon(Icons.arrow_back)),
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Column(
                children: [
                  Container(
                    height: 180,
                    // child: Image.asset('images/logo1.png'),
                    //  child: Image.asset('images/chat.png'),
                    child: Image.asset('images/Robot.png'),
                  ),
                  Text(
                    'Message Bolt',
                    style: TextStyle(
                        fontWeight: FontWeight.w900,
                        fontSize: 40,
                        color: Color.fromARGB(255, 1, 0, 34)),
                  ),
                  SizedBox(height: 30),
                  MyBotton(
                    //color: Colors.blue[800]!,
                    colorStart: Color.fromARGB(255, 100, 224, 255),
                    colorEnd: Color.fromARGB(255, 31, 148, 243),
                    color: [
                      Color.fromARGB(255, 100, 224, 255),
                      Color.fromARGB(255, 31, 148, 243)
                    ],
                    title: TranslationService.of(context)!.translate('Sign in'),
                    onPressed: () {
                      Navigator.pushNamed(context, SigninScreen.screenRoute);
                    },
                  ),
                  SizedBox(height: 20),
                  MyBotton(
                    //color: Colors.blue[800]!,
                    colorStart: Colors.grey,
                    colorEnd: Color.fromARGB(255, 87, 86, 86),
                    color: [Colors.grey, Color.fromARGB(255, 87, 86, 86)],

                    title:
                        TranslationService.of(context)!.translate('Register'),
                    onPressed: () {
                      Navigator.pushNamed(
                          context, RegistrationScreen.screenRoute);
                    },
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
