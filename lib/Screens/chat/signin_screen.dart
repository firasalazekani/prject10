import 'dart:ffi';
//import 'dart:html';

import 'package:clippy_flutter/arc.dart';
import 'package:clustr_map_test_1/servies/local/translation_service.dart';
import 'package:clustr_map_test_1/widgets/local/config/app_config.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

import '../chat/bolt_screen.dart';
import '../chat/chat_screen.dart';
import 'package:clustr_map_test_1/widgets/chat//my_button.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';
import 'package:clustr_map_test_1/widgets/chat//wave_clipper.dart';
import 'package:clustr_map_test_1/widgets/chat//email_validator.dart';

// import 'package:flutter_localizations/flutter_localizations.dart';
// import 'package:location/location.dart';

import 'package:flutter_localizations/flutter_localizations.dart';
//import 'package:flutter_gen/gen_l10n/app_localizations.dart';
//import 'package:url_strategy/url_strategy.dart';
import 'package:provider/provider.dart';

class SigninScreen extends StatefulWidget {
  static const String screenRoute = 'signin_screen';
  const SigninScreen({super.key});

  @override
  State<SigninScreen> createState() => _SigninScreenState();
}

class _SigninScreenState extends State<SigninScreen> {
  final _auth = FirebaseAuth.instance;
  late String email;
  late String password;
  bool showSpinner = false;
  bool _isLoading = false;

  login floginS = new login();

  TextEditingController _namecontrol = TextEditingController();
  TextEditingController _Passwordcontrol = TextEditingController();

  @override
  Widget build(BuildContext context) {
    //final localeProvider = context.watch<LocaleProvider>();
    double defaultPadding = MediaQuery.of(context).size.width * 0.05;
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    // double screenWidt = MediaQuery.of(context).size.width;

    return Scaffold(
        //من اجل تجميد البودي عند الضغط على التكسيت فيلد كي لا يتغير التصميم
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.white,
        body: Stack(
          children: [
            Column(
              //mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,

              children: [
                Flexible(
                  child: Stack(children: [
                    ClipPath(
                      clipper: WaveClipper(),
                      child: Container(
                        // color: Colors.orange[800],
                        // color: Colors.blue[800],
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            colors: [
                              Color.fromARGB(255, 100, 224, 255),
                              Color.fromARGB(255, 31, 148, 243)
                            ],
                            begin: Alignment.topRight,
                            end: Alignment.bottomCenter,
                          ),
                        ),
                        height: 270,
                        alignment: Alignment.centerLeft,
                      ),
                    ),
                    Padding(
                      // padding: const EdgeInsets.fromLTRB(120, 90, 30, 0),

                      padding: const EdgeInsets.fromLTRB(90, 90, 90, 0),
                      child: Image.asset(
                        'images/Robot.png',
                        // 'images/logo1.png',
                        // 'images/chat.png',
                        alignment: Alignment.centerRight,
                        height: screenHeight,
                        width: screenWidth,
                      ),
                    ),
                  ]),
                ),
                Flexible(
                  child: Container(
                    child: Padding(
                      padding: EdgeInsets.fromLTRB(
                          defaultPadding, 0, defaultPadding, 0),
                      child: Column(
                        //  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          // height: 180,
                          SizedBox(
                            height: 33,
                            child: Text(
                              // floginS.errorMessage,
                              TranslationService.of(context)!
                                  .translate(floginS.errorMessage),
                              style: TextStyle(color: Colors.red),
                            ),
                          ),

                          TextField(
                            controller: _namecontrol,
                            keyboardType: TextInputType.emailAddress,
                            textAlign: TextAlign.center,
                            onChanged: (value) {
                              email = value;
                            },
                            decoration: InputDecoration(
                                hintText: TranslationService.of(context)!
                                    .translate('Enter Your Email'),

                                // hintText: 'Enter Your Email',
                                contentPadding: EdgeInsets.symmetric(
                                    horizontal: 10, vertical: 10),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(30),
                                  ),
                                  // borderRadius: BorderRadius.only(
                                  //     topLeft: Radius.circular(30),
                                  //  bottomLeft: Radius.circular(30),
                                  //   bottomRight: Radius.circular(30)),
                                ),
                                enabledBorder: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(30)),
                                  // borderRadius: BorderRadius.only(
                                  //     topLeft: Radius.circular(30),
                                  //     bottomRight: Radius.circular(30)),
                                  borderSide:
                                      //   BorderSide(color: Colors.orange, width: 2),
                                      BorderSide(color: Colors.grey, width: 1),
                                ),
                                focusedBorder: OutlineInputBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(30)),
                                    // BorderRadius.only(
                                    //     topLeft: Radius.circular(30),
                                    //     bottomRight: Radius.circular(30)),
                                    borderSide:
                                        //     BorderSide(color: Colors.blue, width: 3))),
                                        BorderSide(
                                            color: Colors.blue, width: 1))),
                          ),

                          SizedBox(
                            height: 10,
                          ),
                          TextField(
                            controller: _Passwordcontrol,
                            obscureText: true,
                            textAlign: TextAlign.center,
                            onChanged: (value) {
                              password = value;
                            },
                            decoration: InputDecoration(
                              hintText: TranslationService.of(context)!
                                  .translate('Enter Your Password'),

                              // hintText: 'Enter Your Password',
                              contentPadding: EdgeInsets.symmetric(
                                  horizontal: 20, vertical: 10),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(30),
                                  // borderRadius: BorderRadius.only(
                                  //   topLeft: Radius.circular(30),
                                  //   bottomRight: Radius.circular(30),
                                ),
                              ),
                              enabledBorder: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(30)),
                                  // borderRadius: BorderRadius.only(
                                  //     topLeft: Radius.circular(30),
                                  //     bottomRight: Radius.circular(30)),
                                  borderSide:
                                      //   BorderSide(color: Colors.orange, width: 2)),
                                      BorderSide(color: Colors.grey, width: 1)),

                              focusedBorder: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(30)),
                                  // borderRadius: BorderRadius.only(
                                  //     topLeft: Radius.circular(30),
                                  //     bottomRight: Radius.circular(30)),
                                  borderSide:
                                      // BorderSide(color: Colors.blue, width: 3)),
                                      BorderSide(color: Colors.blue, width: 1)),
                            ),
                          ),
                          SizedBox(height: 10),
                          Container(
                            child: MyBotton(
                              //color: Colors.blue[800]!,
                              colorStart: Color.fromARGB(255, 100, 224, 255),
                              colorEnd: Color.fromARGB(255, 31, 148, 243),
                              color: [
                                Color.fromARGB(255, 100, 224, 255),
                                Color.fromARGB(255, 31, 148, 243)
                              ],
                              title: TranslationService.of(context)!
                                  .translate('Sign in'),

                              onPressed: () async {
                                //For hide keybord when pressed on button
                                FocusScope.of(context).unfocus();
                                setState(() {
                                  floginS.FieldRequered(_namecontrol.value.text,
                                      _Passwordcontrol.value.text);
                                  print('IsValid= ${floginS.isVaild}');
                                  print(
                                      'ErrorMessage = ${floginS.errorMessage}');
                                  if (floginS.isVaild == true) {
                                    floginS.validateEmail(email);
                                    //  if (floginS.isVaild == true) {
                                    //   floginS.validatePassword(password);
                                    // }
                                  }
                                  if (floginS.isVaild == true) {
                                    print('IsValid= ${floginS.isVaild}');
                                    print('IsValid= ${floginS.errorMessage}');

                                    //  showSpinner = true;
                                    _isLoading = true;
                                  }
                                });
                                try {
                                  final user =
                                      await _auth.signInWithEmailAndPassword(
                                          email: email, password: password);

                                  //هون اجبرت اليوزر على تفعيل خدمة النوتفيكيشن حتى تصله الاشعارات
                                  FirebaseMessaging.instance
                                      .subscribeToTopic("notification");
                                  // var token = await user.user!.getIdToken();
                                  if (user != null) {
                                    Navigator.pushNamed(
                                        // context, ChatScreen.screenRoute);
                                        context,
                                        BoltScreen.screenRoute);
                                    setState(() {
                                      // print(
                                      //     "==================ToKen User==============");
                                      // print(token);
                                      // print(
                                      //     "==================End ToKen User==============");
                                      // CircularProgressIndicator(
                                      //     color: Colors.white, Text('Loging'));
                                      //showSpinner = false;
                                      _isLoading = false;
                                    });
                                    // print('email : $email');
                                  }
                                }
                                // catch (e) {
                                on FirebaseAuthException catch (e) {
                                  if (e.code == 'user-not-found') {
                                    var error = TranslationService.of(context)!
                                        .translate(
                                            'No user found for that email.');
                                    setState(() {
                                      floginS.errorMessage = error;
                                      _isLoading = false;
                                      // print('No user found for that email.');
                                    });
                                  } else if (e.code == 'wrong-password') {
                                    var error = TranslationService.of(context)!
                                        .translate(
                                            'Wrong password provided for that user.');
                                    setState(() {
                                      floginS.errorMessage = error;
                                      _isLoading = false;
                                      // print(
                                      //     'Wrong password provided for that user.');
                                    });
                                  }
                                  //  print('Error :$e');
                                }
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Flexible(
                  child: ClipPath(
                    clipper: WaveClipperUnder(),
                    child: Container(
                      // color: Colors.blue[800],
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          colors: [
                            Color.fromARGB(255, 31, 148, 243),
                            Color.fromARGB(255, 100, 224, 255)
                          ],
                          begin: Alignment.topRight,
                          end: Alignment.topLeft,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
              //   ),
            ),
            if (_isLoading == true)
              Container(
                  width: MediaQuery.of(context).size.width,
                  color: Color.fromARGB(255, 41, 181, 216).withOpacity(0.3),
                  child: AlertDialog(
                    content: new Row(
                      children: [
                        CircularProgressIndicator(),
                        Container(
                            margin: EdgeInsets.only(left: 7),
                            //   child: Text("Loading...")),
                            child: Text(TranslationService.of(context)!
                                .translate('Loading...'))),
                      ],
                    ),
                  )),
          ],
        )
        //:

        //  Text('Please Loging'),
        // ),

        );
  }
}

// void Circul() {
//   CircularProgressIndicator(color: Colors.white, Tex
// }
