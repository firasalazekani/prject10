import 'package:clustr_map_test_1/Screens/home_screen.dart';
import 'package:clustr_map_test_1/servies/local/translation_service.dart';
import 'package:clustr_map_test_1/widgets/local/config/app_config.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../chat/signin_screen.dart';
import '../chat/welcome_screen.dart';

import 'package:clustr_map_test_1/widgets/chat//wave_clipper.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:avatar_glow/avatar_glow.dart';
//import 'package:speech_to_text/speech_to_text.dart' as stts;
import 'package:speech_to_text/speech_to_text.dart';

import 'package:speech_to_text/speech_recognition_error.dart';
import 'package:speech_to_text/speech_recognition_result.dart';
import 'dart:math' as math;

int coutner = 0;
final _firestor = FirebaseFirestore.instance;
//late User signedInUser; // هي رح تعطينا اميل
bool isRobot = false;
bool boolDisgin = true;

class ChatScreen extends StatefulWidget {
  static const String screenRoute = 'chat_screen';
  const ChatScreen({super.key});

  @override
  State<ChatScreen> createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  final messageTextController = TextEditingController();
  final _auth = FirebaseAuth.instance;
  final storage = new FlutterSecureStorage();
  Locale _currentLocale = const Locale(AppConfig.defaultLanguage, '');
  bool boolLanguage = true;
  Future<void> GetLanguage() async {
    // final prefs = await SharedPreferences.getInstance();
    // final String? lang = prefs.getString('lang');
    final String? lang = await storage.read(key: 'jwt');
    setState(() {
      boolDisgin = boolLanguage;
    });
    print('Hi Hi === $lang');
    if (lang == "ar") {
      setState(() {
        boolLanguage = false;
        //  boolDisgin = false;
        boolDisgin = boolLanguage;

        print('boolLanguage =$boolLanguage');
        print('boolDisany =$boolDisgin');
      });
    }
  }

  String? messageText; //هي رح تعطينا رسالة
  //متحولات البحث
  var SearchWord = '';
  List<String> listSearchWord = [
    'hi',
    'how are you',
    'Please',
    'what are you doing',
    "مرحبا كيف حالك",
  ];
  List<String> listResultCombin = [
    'Hi Mrs',
    'I am Fine Thank you And you?',
    'I am waiting for any question you want me to answer',
    'Nothing, I am waiting for you',
    "انا بخير, كيف حالك انت",
  ];
  //نهاية متحولات البحث

//تابع التسجيل
  //var _speechToText = stts.SpeechToText();

  bool isliseting = false;
  String text = 'Press The Button for Speacking';

  final SpeechToText _speechToText = SpeechToText();
  bool _speechEnabled = false;
  bool _speechAvailable = false;
  String _lastWords = '';
  String _currentWords = '';
  final String _selectedLocaleId = 'es_MX';

  void SearchWordInList(String Word) {
    for (var i = 0; i < listSearchWord.length; i++) {
      if (SearchWord.toLowerCase().contains(listSearchWord[i].toLowerCase())) {
        _firestor.collection('message').add(
          {
            'text': listResultCombin[i],
            'sender': 'rebort@test.com',
            'receiver': signedInUser.email,
            'isbolt': true,

            'time': FieldValue.serverTimestamp(),
            // 'receiver': signedInUser,
          },
        );
      }
    }
  }

  void errorListener(SpeechRecognitionError error) {
    debugPrint(error.errorMsg.toString());
  }

// you need change in Andriod-app-main-AndriosManifest.xml
// minSdkVersion 24
  //     targetSdkVersion 28

  void statusListener(String status) async {
    debugPrint("status $status");
    if (status == "done" && _speechEnabled) {
      setState(() {
        _lastWords += " $_currentWords";
        _currentWords = "";
        _speechEnabled = false;
        // isliseting = false;
      });
      await _startListening();
    }
  }

  Future _stopListening() async {
    setState(() {
      _speechEnabled = false;

      //  isliseting = false;
    });
    await _speechToText.stop();
  }

  /// This is the callback that the SpeechToText plugin calls when
  /// the platform returns recognized words.
  void _onSpeechResult(SpeechRecognitionResult result) {
    setState(() {
      _currentWords = result.recognizedWords;
    });
  }

  /// Each time to start a speech recognition session
  Future _startListening() async {
    debugPrint("=================================================");
    await _stopListening();
    await Future.delayed(const Duration(milliseconds: 100));
    await _speechToText.listen(
        onResult: _onSpeechResult,
        cancelOnError: false,
        partialResults: true,
        listenMode: ListenMode.dictation);
    setState(() {
      _speechEnabled = true;
      // isRobot = true;
    });
  }

  void _initSpeech() async {
    _speechAvailable = await _speechToText.initialize(
        onError: errorListener, onStatus: statusListener);
    setState(() {});
  }

  void write() {
    _stopListening();
    // isRobot = false;
    setState(() {
      messageTextController.clear();
      _firestor.collection('message').add(
        {
          'text': _lastWords + _currentWords,
          'sender': signedInUser.email,
          'receiver': signedInUser.email,
          'isbolt': false,
          'time': FieldValue.serverTimestamp(),
        },
      );

      SearchWord = _lastWords + _currentWords;
      SearchWordInList(SearchWord);
      _lastWords = '';
      _currentWords = '';
    });
  }
  // void listen() async {
  //   if (!isliseting) {
  //     bool available = await _speechToText.initialize(
  //         onStatus: statusListener, onError: errorListener);
  //     if (available) {
  //       setState(() {
  //         isliseting = true;
  //       });
  //       _startListening();
  //     }
  //   } else {
  //     setState(() {
  //       isliseting = false;
  //     });
  //     _stopListening();
  //     _speechToText.stop();

  //     setState(() {
  //       messageTextController.clear();
  //       _firestor.collection('message').add(
  //         {
  //           'text': _lastWords + _currentWords,
  //           'sender': signedInUser.email,
  //           'time': FieldValue.serverTimestamp(),
  //         },
  //       );

  //       SearchWord = _lastWords + _currentWords;
  //       SearchWordInList(SearchWord);
  //       // messageTextController.clear();
  //     });
  //     _lastWords = '';
  //     _currentWords = '';
  //   }
  // }

  // void getMessages() async {
  //   final mymessage = await _firestor.collection('message').get();
  //   for (var messa in mymessage.docs) {
  //     print(messa.data());
  //   }
  // }
  // void StreamMessage() async {
  //   await for (var snapshot in _firestor.collection('message').snapshots()) {
  //     for (var message in snapshot.docs) {
  //       print(message.data());
  //     }
  //   }
  // }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getCurrentUser();
    _initSpeech();
    GetLanguage();
    //listen();
    // _speechToText = stts.SpeechToText();
  }

  void getCurrentUser() {
    try {
      final user = _auth.currentUser;
      if (user != null) {
        signedInUser = user;
        print(signedInUser.email);
      }
    } catch (e) {
      print('e :$e');
    }
  }

  void FalseRobot() {
    isRobot = false;
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        // backgroundColor: Colors.blue[800]!,
        backgroundColor: Colors.blue,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            // Image.asset('images/logo1.png', height: 25),
            // SizedBox(width: 20),
            Text('Chatbot'),

            // Alignment(x, y)
          ],
        ),
        actions: [
          IconButton(
              onPressed: () {
                _auth.signOut();
                //من اجل وقت يعمل تسجيل خروج يعمل الغاء كي لا تصله الاشعارات
                //وقت يعمل تسجيل دخول بنرد بنفعل هذه الميزة
                FirebaseMessaging.instance.unsubscribeFromTopic("notification");

                Navigator.pushNamed(context, HomeScreen.screenRoute);
              },
              icon: Icon(Icons.close)),
          IconButton(
              onPressed: () {
                //_auth.signOut();
                //من اجل وقت يعمل تسجيل خروج يعمل الغاء كي لا تصله الاشعارات
                //وقت يعمل تسجيل دخول بنرد بنفعل هذه الميزة
                // FirebaseMessaging.instance.unsubscribeFromTopic("notification");

                Navigator.popAndPushNamed(context, HomeScreen.screenRoute);
              },
              icon: Icon(Icons.home)),
        ],
      ),
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            MessageStreamBilder(),
            Container(
              //width: 100,
              //لرسم خط مستقيم
              decoration: BoxDecoration(
                border: Border(
                  top: BorderSide(
                    //  color: Colors.orange,
                    color: Color.fromARGB(255, 100, 224, 255),
                    width: 2,
                  ),
                ),
              ),
              //----End firas
              // color: Colors.orange,
              child: boolLanguage == true
                  ? Stack(
                      children: [
                        Row(
                          // child: Stack(
                          // crossAxisAlignment: CrossAxisAlignment.stretch,
                          //  mainAxisAlignment: MainAxisAlignment.center,
                          //  crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Expanded(
                              child: TextField(
                                style: TextStyle(fontSize: 15),
                                controller: messageTextController,
                                onChanged: (value) {
                                  messageText = value;
                                },
                                decoration: InputDecoration(
                                    contentPadding: EdgeInsets.symmetric(
                                        vertical: 20, horizontal: 10),
                                    hintText: TranslationService.of(context)!
                                        .translate('Write your Message here'),
                                    //  hintText: 'Write your Message here',
                                    border: InputBorder.none),
                              ),
                            ),
                            // Container(
                            // SizedBox(
                            //   width: 20,
                            // ),
                            //  Stack(
                            //  children: [
                            Padding(
                              // padding: const EdgeInsets.fromLTRB(0, 20, 50, 0),
                              padding: const EdgeInsets.only(top: 20),
                              child: IconButton(
                                onPressed: _speechToText.isNotListening
                                    ? _startListening
                                    : write,
                                // onPressed: (_speechToText.isNotListening) {
                                //  // if (_speechToText.isNotListening == true) {
                                //     _startListening;
                                //    if(_speechToText.isListening) {
                                //     _stopListening;
                                //   }
                                // }
                                //tooltip: 'Listen',
                                //listen();

                                icon: Padding(
                                  padding: const EdgeInsets.only(left: 30),
                                  //  padding: const EdgeInsets.fromLTRB(30, 0, 10, 30),

                                  child: Icon(_speechToText.isNotListening
                                      ? Icons.mic_off
                                      : Icons.mic),
                                ),
                                color: Colors.blue,
                                alignment: Alignment.topLeft,

                                // ),
                              ),
                            ),
                            // Padding(padding: EdgeInsets.only(top: 100.0)),
                            //  ),
                            // Expanded(
                            // child: Stack(
                            //   children: [
                            // child:
                            // Spacer(),
                            ClipPath(
                              clipper: WaveClipperButton(),
                              child: Container(
                                height: 80,
                                width: 90,
                                // alignment: Alignment.center,
                                // color: Colors.orange[800],
                                decoration: BoxDecoration(
                                  gradient: LinearGradient(
                                    colors: [
                                      Color.fromARGB(255, 100, 224, 255),
                                      Color.fromARGB(255, 31, 148, 243),
                                    ],
                                    begin: Alignment.topRight,
                                    end: Alignment.bottomCenter,
                                  ),
                                ),
                                //   height: 270,
                                alignment: Alignment.centerRight,

                                child: TextButton(
                                  //style: TextStyle(fontSize: 20),
                                  onPressed: () {
                                    if (messageText != '') {
                                      messageTextController.clear();
                                      _firestor.collection('message').add(
                                        {
                                          'text': messageText,
                                          //  'text': text,
                                          'sender': signedInUser.email,
                                          'receiver': signedInUser.email,
                                          'isbolt': false,

                                          'time': FieldValue.serverTimestamp(),
                                          // 'receiver': signedInUser,
                                        },
                                      );
                                      _firestor.collection('notification').add(
                                        {
                                          'text': messageText,
                                          'sender': signedInUser.email,
                                          // 'receiver': signedInUser.email,
                                          //'isbolt': false,
                                          //'time': FieldValue.serverTimestamp(),
                                        },
                                      );
                                      SearchWord = messageText.toString();
                                      SearchWordInList(SearchWord);
                                      //  messageTextController.clear();
                                      messageText = '';
                                    }
                                  },
                                  child: Icon(
                                    Icons.send,
                                    color: Colors.white,
                                  ),

                                  // 'Send',
                                  // style: TextStyle(
                                  //   color: Colors.blue[800],
                                  //   fontWeight: FontWeight.bold,
                                  //   fontSize: 18,
                                  //),
                                ),
                              ),
                            ),
                            // ),
                            //   ],
                            // ),
                            //  ),
                            // Expanded(
                            //   child:

                            // ),

                            //  ],
                            // ),

                            // Container(
                            //   width: 20,
                            //   height: 64,
                            //   color: Colors.orange[800],
                            // ),
                          ],
                        ),
                      ],
                    )
                  : Stack(
                      children: [
                        Row(
                          // child: Stack(
                          // crossAxisAlignment: CrossAxisAlignment.stretch,
                          //  mainAxisAlignment: MainAxisAlignment.center,
                          //  crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Expanded(
                              child: TextField(
                                style: TextStyle(fontSize: 15),
                                controller: messageTextController,
                                onChanged: (value) {
                                  messageText = value;
                                },
                                decoration: InputDecoration(
                                    contentPadding: EdgeInsets.symmetric(
                                        vertical: 20, horizontal: 10),
                                    hintText: TranslationService.of(context)!
                                        .translate('Write your Message here'),
                                    //  hintText: 'Write your Message here',
                                    border: InputBorder.none),
                                textAlign: TextAlign.right,
                              ),
                            ),
                            Padding(
                              // padding: const EdgeInsets.fromLTRB(0, 20, 50, 0),
                              padding: const EdgeInsets.only(top: 20),
                              child: IconButton(
                                onPressed: _speechToText.isNotListening
                                    ? _startListening
                                    : write,
                                // onPressed: (_speechToText.isNotListening) {
                                //  // if (_speechToText.isNotListening == true) {
                                //     _startListening;
                                //    if(_speechToText.isListening) {
                                //     _stopListening;
                                //   }
                                // }
                                //tooltip: 'Listen',
                                //listen();

                                icon: Padding(
                                  padding: const EdgeInsets.only(right: 20.0),
                                  //  padding: const EdgeInsets.fromLTRB(30, 0, 10, 30),

                                  child: Icon(_speechToText.isNotListening
                                      ? Icons.mic_off
                                      : Icons.mic),
                                ),
                                color: Colors.blue,
                                alignment: Alignment.topRight,

                                // ),
                              ),
                            ),
                            Transform(
                              // angle: 180 * math.pi / 180,
                              transform: Matrix4.rotationY(math.pi),
                              alignment: Alignment.center,
                              child: ClipPath(
                                clipper: WaveClipperButton(),
                                child: Container(
                                  height: 80,
                                  width: 90,
                                  // alignment: Alignment.center,
                                  // color: Colors.orange[800],
                                  decoration: BoxDecoration(
                                    gradient: LinearGradient(
                                      colors: [
                                        Color.fromARGB(255, 100, 224, 255),
                                        Color.fromARGB(255, 31, 148, 243),
                                      ],
                                      begin: Alignment.topRight,
                                      end: Alignment.bottomCenter,
                                    ),
                                  ),
                                  //   height: 270,
                                  alignment: Alignment.centerRight,

                                  child: TextButton(
                                    //style: TextStyle(fontSize: 20),
                                    onPressed: () {
                                      if (messageText != '') {
                                        messageTextController.clear();
                                        _firestor.collection('message').add(
                                          {
                                            'text': messageText,
                                            //  'text': text,
                                            'sender': signedInUser.email,
                                            'receiver': signedInUser.email,
                                            'isbolt': false,

                                            'time':
                                                FieldValue.serverTimestamp(),
                                            // 'receiver': signedInUser,
                                          },
                                        );
                                        _firestor
                                            .collection('notification')
                                            .add(
                                          {
                                            'text': messageText,
                                            'sender': signedInUser.email,
                                            // 'receiver': signedInUser.email,
                                            //'isbolt': false,
                                            //'time': FieldValue.serverTimestamp(),
                                          },
                                        );
                                        SearchWord = messageText.toString();
                                        SearchWordInList(SearchWord);
                                        //  messageTextController.clear();
                                        messageText = '';
                                      }
                                    },
                                    child: Transform.rotate(
                                      angle: 180 * math.pi / 180,
                                      child: IconButton(
                                        icon: Icon(
                                          Icons.send,
                                          color: Colors.white,
                                        ),
                                        onPressed: null,
                                      ),
                                    ),

                                    // 'Send',
                                    // style: TextStyle(
                                    //   color: Colors.blue[800],
                                    //   fontWeight: FontWeight.bold,
                                    //   fontSize: 18,
                                    //),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
            ),
            //  FalseRobot,
          ],
        ),
      ),
    );
  }
}

//من اجل الشكل تبع الرسالة المرسلة
class MessageLine extends StatelessWidget {
  const MessageLine(
      {this.text,
      this.sender,
      this.isbolt,
      this.receiver,
      required this.isMe,
      Key? key})
      : super(key: key);
  final String? text;
  final String? sender;
  final bool? isbolt;
  final String? receiver;
  final bool isMe;
  //final bool isRobot;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(10.0),
      child: Column(
        crossAxisAlignment:
            isMe ? CrossAxisAlignment.end : CrossAxisAlignment.start,
        children: [
          isMe
              ? Material(
                  elevation: 5,
                  borderRadius: boolDisgin
                      ? BorderRadius.only(
                          topLeft: Radius.circular(30),
                          bottomLeft: Radius.circular(30),
                          bottomRight: Radius.circular(30))
                      : BorderRadius.only(
                          topRight: Radius.circular(30),
                          bottomLeft: Radius.circular(30),
                          bottomRight: Radius.circular(30),
                        ), //
                  color: isMe ? Colors.blue[800] : Colors.white,
                  child: Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 10.0, horizontal: 20.0),
                      child: Text(
                        '$text',
                        style: TextStyle(
                            fontSize: 20,
                            color: isMe ? Colors.white : Colors.black45),
                      )),
                )
              : Row(
                  children: [
                    CircleAvatar(
                      radius: 20.0,
                      backgroundImage: AssetImage('images/Robot.png'),
                      backgroundColor: Colors.white,
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Expanded(
                      child: Material(
                        elevation: 5,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            '$text',
                            style: TextStyle(
                              fontSize: 20,
                              color: isMe ? Colors.white : Colors.black45,
                            ),
                          ),
                        ),
                        borderRadius: boolDisgin
                            ? BorderRadius.only(
                                topRight: Radius.circular(30),
                                bottomLeft: Radius.circular(30),
                                bottomRight: Radius.circular(30),
                              )
                            : BorderRadius.only(
                                topLeft: Radius.circular(30),
                                bottomLeft: Radius.circular(30),
                                bottomRight: Radius.circular(30)),
                      ),
                    ),
                  ],
                ),
        ],
      ),
    );
  }
}

class MessageLineArabic extends StatelessWidget {
  const MessageLineArabic(
      {this.text,
      this.sender,
      this.isbolt,
      this.receiver,
      required this.isMe,
      Key? key})
      : super(key: key);
  final String? text;
  final String? sender;
  final bool? isbolt;
  final String? receiver;
  final bool isMe;
  //final bool isRobot;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(10.0),
      child: Column(
        crossAxisAlignment:
            isMe ? CrossAxisAlignment.end : CrossAxisAlignment.start,
        children: [
          isMe
              ? Material(
                  elevation: 5,
                  borderRadius: BorderRadius.only(
                      // topLeft: Radius.circular(30),
                      topRight: Radius.circular(30),
                      bottomLeft: Radius.circular(30),
                      bottomRight: Radius.circular(30)),
                  // : BorderRadius.only(
                  //     topRight: Radius.circular(30),
                  //     bottomLeft: Radius.circular(30),
                  //     bottomRight: Radius.circular(30),
                  //   ),
                  color: isMe ? Colors.blue[800] : Colors.white,
                  child: Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 10.0, horizontal: 20.0),
                      child: Text(
                        '$text',
                        style: TextStyle(
                            fontSize: 20,
                            color: isMe ? Colors.white : Colors.black45),
                      )),
                )
              : Row(
                  children: [
                    CircleAvatar(
                      radius: 20.0,
                      backgroundImage: AssetImage('images/Robot.png'),
                      backgroundColor: Colors.white,
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Expanded(
                      child: Material(
                        elevation: 5,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            '$text',
                            style: TextStyle(
                              fontSize: 20,
                              color: isMe ? Colors.white : Colors.black45,
                            ),
                          ),
                        ),
                        borderRadius: BorderRadius.only(
                          topRight: Radius.circular(30),
                          bottomLeft: Radius.circular(30),
                          bottomRight: Radius.circular(30),
                        ),
                      ),
                    ),
                  ],
                ),
        ],
      ),
    );
  }
}

//من اجل بناء ليييست الرسائل
class MessageStreamBilder extends StatelessWidget {
  //const MyWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: _firestor.collection('message').orderBy('time').snapshots(),
      builder: (context, snapshot) {
        List<MessageLine> messageWidgets = [];
        if (!snapshot.hasData) {
          return Center(
            child: CircularProgressIndicator(
              backgroundColor: Colors.blue,
            ),
          );
        }
        final messages = snapshot.data!.docs.reversed;
        for (var message in messages) {
          final messageText = message.get('text');
          final messageSender = message.get('sender');
          final currentUser = signedInUser.email;
          final messageIsBolt = message.get('isbolt');
          final messageReceiver = message.get('receiver');

          final messageWidget = MessageLine(
            text: messageText,
            sender: messageSender,
            isMe: currentUser == messageSender,
            isbolt: messageIsBolt,
            receiver: messageReceiver,
          );
          if (currentUser == messageSender) {
            messageWidgets.add(messageWidget);
          }
          if (messageIsBolt == true && messageReceiver == currentUser) {
            messageWidgets.add(messageWidget);
          }
        }
        return Expanded(
          child: ListView(
            reverse: true,
            padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
            children: messageWidgets,
          ),
        );
      },
    );
  }
}
