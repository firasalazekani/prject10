import 'dart:convert';

import 'package:clustr_map_test_1/servies/local/translation_service.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

import '../chat/bolt_screen.dart';
import '../chat/chat_screen.dart';

import 'package:clustr_map_test_1/widgets/chat/my_button.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';
import 'package:clustr_map_test_1/widgets/chat//wave_clipper.dart';
import 'package:clustr_map_test_1/widgets/chat//email_validator.dart';

class RegistrationScreen extends StatefulWidget {
  static const String screenRoute = 'registration_screen';
  const RegistrationScreen({super.key});

  @override
  State<RegistrationScreen> createState() => _RegistrationScreenState();
}

class _RegistrationScreenState extends State<RegistrationScreen> {
  final _auth = FirebaseAuth.instance;
  late String email;
  late String password;

  bool showSpinner = false;
  login flogin = new login();

  TextEditingController _namecontrol = TextEditingController();
  TextEditingController _Passwordcontrol = TextEditingController();
  @override
  @override

  // GestureDetector(
  //من اجل اخفاء الكبيورد عند النقر على اي مكان خارج حقول الادخال
  Widget build(BuildContext context) {
    double defaultPadding = MediaQuery.of(context).size.width * 0.05;
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        //من اجل تجميد البودي عند الضغط على التكسيت فيلد كي لا يتغير التصميم
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.white,
        body: Stack(children: [
          // showSpinner
          Column(
            children: [
              Flexible(
                child: Stack(children: [
                  ClipPath(
                    clipper: WaveClipper(),
                    child: Container(
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          colors: [
                            Color.fromARGB(255, 100, 224, 255),
                            Color.fromARGB(255, 31, 148, 243)
                          ],
                          begin: Alignment.topRight,
                          end: Alignment.bottomCenter,
                        ),
                      ),
                      height: 270,
                      alignment: Alignment.centerLeft,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(90, 90, 90, 0),
                    child: Image.asset(
                      // 'images/logo1.png',
                      'images/Robot.png',
                      alignment: Alignment.centerRight,
                      height: screenHeight,
                      width: screenWidth,
                    ),
                  ),
                ]),
              ),
              Flexible(
                child: Padding(
                  padding:
                      EdgeInsets.fromLTRB(defaultPadding, 0, defaultPadding, 0),
                  child: Container(
                    child: Column(children: [
                      SizedBox(
                          height: 40,
                          child: Text(
                            TranslationService.of(context)!
                                .translate('${flogin.errorMessage}'),
                            style: TextStyle(color: Colors.red),
                          )),
                      TextField(
                        controller: _namecontrol,
                        keyboardType: TextInputType.emailAddress,
                        textAlign: TextAlign.center,
                        onChanged: (value) {
                          email = value;
                          // setState(() {
                          //   flogin.validateEmail(value);
                          // });
                        },
                        decoration: InputDecoration(
                          hintText: TranslationService.of(context)!
                              .translate('Enter Your Email'),
                          //hintText: 'Enter Your Email',
                          contentPadding: EdgeInsets.symmetric(
                              horizontal: 20, vertical: 10),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(30),
                            ),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide:
                                //  BorderSide(width: 2, color: Colors.orange),
                                BorderSide(color: Colors.grey, width: 1),
                            borderRadius: BorderRadius.all(Radius.circular(30)),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(30)),
                            borderSide:
                                BorderSide(width: 1, color: Colors.blue),
                            //  BorderSide(color: Colors.blue, width: 3),
                          ),
                        ),
                      ),
                      SizedBox(height: 10),
                      TextField(
                        controller: _Passwordcontrol,
                        obscureText: true,
                        textAlign: TextAlign.center,
                        onChanged: (value) {
                          password = value;
                        },
                        decoration: InputDecoration(
                          hintText: TranslationService.of(context)!
                              .translate('Enter Your Password'),
                          // hintText: 'Enter Your Password',
                          contentPadding: EdgeInsets.symmetric(
                              horizontal: 10, vertical: 10),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(30),
                            ),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(30)),
                            borderSide:
                                // BorderSide(color: Colors.orange, width: 2),
                                BorderSide(color: Colors.grey, width: 1),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(30)),
                            borderSide:
                                BorderSide(color: Colors.blue, width: 1),
                            //   BorderSide(color: Colors.blue, width: 3),
                          ),
                        ),
                      ),
                      SizedBox(height: 10),
                      MyBotton(
                          //   color: Colors.blue[800]!,
                          colorStart: Color.fromARGB(255, 100, 224, 255),
                          colorEnd: Color.fromARGB(255, 31, 148, 243),
                          color: [
                            Color.fromARGB(255, 100, 224, 255),
                            Color.fromARGB(255, 31, 148, 243)
                          ],
                          title: TranslationService.of(context)!
                              .translate('Registration'),
                          // title: 'Registration',
                          onPressed: () async {
                            FocusScope.of(context).unfocus();
                            setState(() {
                              flogin.FieldRequered(_namecontrol.value.text,
                                  _Passwordcontrol.value.text);
                              print('IsValid= ${flogin.isVaild}');
                              print('ErrorMessage = ${flogin.errorMessage}');
                              if (flogin.isVaild == true) {
                                flogin.validateEmail(email);
                                flogin.validateEmailDomain(email);
                                if (flogin.isVaild == true) {
                                  flogin.validatePassword(password);
                                  flogin.validatePasswordDomain(password);
                                }
                              }
                              if (flogin.isVaild == true) {
                                print('IsValid= ${flogin.isVaild}');
                                print('IsValid= ${flogin.errorMessage}');
                                showSpinner = true;
                              }
                            });
                            try {
                              if (flogin.isVaild == true) {
                                final newUser =
                                    await _auth.createUserWithEmailAndPassword(
                                        email: email, password: password);
                                //هذه الخاصية من اجل اقدر ابعتله اشعار
                                //لانو بهل لحالة صار مشترك بالاشعارات
                                FirebaseMessaging.instance
                                    .subscribeToTopic("notification");
                                Navigator.pushNamed(
                                    context, BoltScreen.screenRoute);
                                setState(() {
                                  // CircularProgressIndicator(
                                  //   value: 50,
                                  //   semanticsLabel: 'Loading',
                                  //   backgroundColor: Colors.green,
                                  // );

                                  showSpinner = false;
                                });
                              }
                            } on FirebaseAuthException catch (e) {
                              setState(
                                () {
                                  if (e.code == 'email-already-in-use') {
                                    flogin.errorMessage = TranslationService.of(
                                            context)!
                                        .translate(
                                            'The account already exists for that email.');
                                    //  'The account already exists for that email.';
                                    showSpinner = false;
                                  }
                                },
                              );
                              print('Error when Registar New User $e');
                            }
                            // }
                          }),
                    ]),
                  ),
                ),
              ),
              Expanded(
                child: ClipPath(
                  clipper: WaveClipperUnder(),
                  child: Container(
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        colors: [
                          Color.fromARGB(255, 31, 148, 243),
                          Color.fromARGB(255, 100, 224, 255)
                        ],
                        begin: Alignment.topRight,
                        end: Alignment.topLeft,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),

          if (showSpinner == true)
            //   print('showSpinner= $showSpinner');
            Container(
              //  width: MediaQuery.of(context).size.width,
              color: Color.fromARGB(255, 41, 181, 216).withOpacity(0.3),
              child: AlertDialog(
                content: new Row(
                  children: [
                    CircularProgressIndicator(),
                    Container(
                        margin: EdgeInsets.only(left: 10),
                        // child: Text("Loading...")),
                        child: Text(TranslationService.of(context)!
                            .translate('Loading...'))),
                  ],
                ),
              ),
            ),
          // ),
          // }
          // ),
          //),
        ]),
        // ),
      ),
    );
  }
}
