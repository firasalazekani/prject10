import 'dart:async';
import 'dart:convert';
//import 'dart:html';

import 'package:clustr_map_test_1/Screens/chat/welcome_screen.dart';
import 'package:clustr_map_test_1/Screens/home_screen.dart';
import 'package:clustr_map_test_1/servies/local/translation_service.dart';
import 'package:clustr_map_test_1/widgets/chat/my_button.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
//import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:http/http.dart' as http;
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

//import 'firebase_options.dart';
//import 'message.dart';
//import 'message_list.dart';
////import 'permissions.dart';
//import 'token_monitor.dart';

final _firestor = FirebaseFirestore.instance;
//late User signedInUser1;
final _auth = FirebaseAuth.instance;

class NotifactionServer extends StatefulWidget {
  static const String screenRoute = 'notifaction_server';

  const NotifactionServer({super.key});

  @override
  State<NotifactionServer> createState() => _NotifactionServerState();
}

class _NotifactionServerState extends State<NotifactionServer> {
  var serverToken =
      "AAAAWmOe574:APA91bHVBxATiKWpxuQXm8_bc8Ub3EHQQ9DoBgsihtnNR7LkKfUh7VcWxzoMA1t8xemv93-R5lMpNXLbRbVxQR8O8vLpbGtAw9CUdGHrFq02zOG7DpctiFInVLGm1vEyyuYyA4Y3P2XS";
  // "AAAAWmOe574:APA91bHVBxATiKWpxuQXm8_bc8Ub3EHQQ9DoBgsihtnNR7LkKfUh7VcWxzoMA1t8xemv93-R5lMpNXLbRbVxQR8O8vLpbGtAw9CUdGHrFq02zOG7DpctiFInVLGm1vEyyuYyA4Y3P2XS";
  // "AAAADbsL-Dg:APA91bElC80VQ73NvMLqVLuv1kPBJczRNsYzD9peP5oWM4dWcR6ao6QSqqdme2JGqw7Yl5sB88zeI6MgWtkpHM9bPJ6NGHZAeghQCSTDMIboh20I2NTsOFmrWh2dFeJ9Rl_89uFUZTLX";
  //var serverToken = "AIzaSyCYK3r5wwVd_pWlkyxFzolduWXA9lL3a_E";
  late String NotificationTitle;
  late String NotficationText;
  late String FullNameSender;
  bool LogUser = false;
  sendNotfiy(String title, String body, String id, String FullName) async {
    await http.post(Uri.parse('https://fcm.googleapis.com/fcm/send'),
        // Uri.parse('https://api.rnfirebase.io/messaging/send'),
        headers: <String, String>{
          'Content-Type': 'application/json',
          'Authorization': 'key=$serverToken',
        },
        body: jsonEncode(<String, dynamic>{
          'notification': <String, dynamic>{
            'body': body.toString(),
            'title': title.toString()
          },
          'priority': 'high',
          'data': <String, dynamic>{
            'click_action': 'FLUTTER_NOTIFICATION_CLICK',
            'id': id.toString(),
            //"name": "Firas",
            "name": FullName.toString(),
            // "lastname": lastname.toString(),
          },
          //التوكين الذي اريد ارسال هذه النوتيفكيشن اليه
          //وفي هذه الحال انا سوف ارسل الى جهازي لانو ما عندي توكن ارسل اليه
          // 'to': await FirebaseMessaging.instance.getToken()
          //'to': '/topics/firas'
          'to': '/topics/notification'
        }));
  }

  getMessage() {
    FirebaseMessaging.onMessage.listen((event) {
      print("==========================");
      print(event.notification!.title);
      print(event.notification!.body);
      print(event.data["name"]);
      // print(event.data["lastname"]);
    });
  }

  //final user = _auth.currentUser;

  // void getCurrentUser() {
  //   try {
  //     final user = _auth.currentUser;
  //     if (user != null) {
  //       signedInUser = user;
  //       LogUser = true;
  //       print(signedInUser.email);
  //     } else {
  //       //  Navigator.pushNamed(context, WelcomeScreen.screenRoute);
  //     }
  //   } catch (e) {
  //     print('e :$e');
  //   }
  // }

  @override
  void initState() {
    // getCurrentUser();

    getMessage();

    // TODO: implement initState
    super.initState();
  }

  TextEditingController _NotificationTitle = TextEditingController();
  TextEditingController _NotificationText = TextEditingController();
  TextEditingController _FullNameSender = TextEditingController();
  //TextEditingController _NotificationText = TextEditingController();
  @override
  Widget build(BuildContext context) {
    double defaultPadding = MediaQuery.of(context).size.width * 0.05;
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          // title: Text('Notification ', textAlign: TextAlign.center),
          title: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              // Image.asset('images/logo1.png', height: 25),
              // SizedBox(width: 20),
              Text(TranslationService.of(context)!.translate('Notification')),

              // Alignment(x, y)
            ],
          ),
          actions: [
            IconButton(
                onPressed: () {
                  _auth.signOut();
                  //من اجل وقت يعمل تسجيل خروج يعمل الغاء كي لا تصله الاشعارات
                  //وقت يعمل تسجيل دخول بنرد بنفعل هذه الميزة
                  FirebaseMessaging.instance
                      .unsubscribeFromTopic("notification");
                  // Navigator.of(context, rootNavigator: true).pop();
                  // Navigator.pushNamed(context, HomeScreen.screenRoute);
                  // Navigator.pop(context, HomeScreen.screenRoute);
                  //  Navigator.of(context).pop();
                  //  Navigator.of(context).maybePop();
                  Navigator.of(context)
                      .pushNamed(HomeScreen.screenRoute)
                      .then((value) => setState(() {}));
                },
                icon: Icon(Icons.close)),
            IconButton(
                onPressed: () {
                  //  Navigator.of(context, rootNavigator: true).pop();
                  // Navigator.pop(context, HomeScreen.screenRoute);
                  //  Navigator.pushNamed(context, HomeScreen.screenRoute);
                  //   Navigator.pop(context);
                  //  Navigator.of(context).pop();
                  // Navigator.of(context).maybePop();
                  // Navigator.pop(context, HomeScreen.screenRoute);
                  Navigator.of(context)
                      .pushNamed(HomeScreen.screenRoute)
                      .then((value) => setState(() {}));
                },
                icon: Icon(Icons.home)),
          ],
          leading: IconButton(
              onPressed: () {
                Navigator.pushNamed(context, HomeScreen.screenRoute);
              },
              icon: Icon(Icons.arrow_back)),
        ),

        // body: LogUser
        body: Column(
          children: [
            SizedBox(
              height: 30,
            ),
            Text(
              (TranslationService.of(context)!.translate('Notification Title')),
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: 3,
            ),
            Padding(
              padding:
                  EdgeInsets.fromLTRB(defaultPadding, 0, defaultPadding, 0),
              child: TextField(
                controller: _NotificationTitle,
                textAlign: TextAlign.center,
                onChanged: (value) {
                  NotificationTitle = value;
                },
                decoration: InputDecoration(
                    hintText: TranslationService.of(context)!
                        .translate('Enter Your Notification Title'),

                    // hintText: 'Enter Your Email',
                    contentPadding:
                        EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(30),
                      ),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(30)),
                      borderSide: BorderSide(color: Colors.grey, width: 1),
                    ),
                    focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(30)),
                        borderSide: BorderSide(color: Colors.blue, width: 1))),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              TranslationService.of(context)!.translate('Notification Text'),
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: 3,
            ),
            Padding(
              padding:
                  EdgeInsets.fromLTRB(defaultPadding, 0, defaultPadding, 0),
              child: TextField(
                controller: _NotificationText,
                textAlign: TextAlign.center,
                onChanged: (value) {
                  NotficationText = value;
                  //Size.fromHeight(25)
                },
                decoration: InputDecoration(
                    hintText: TranslationService.of(context)!
                        .translate('Enter Your Notification Text'),

                    // hintText: 'Enter Your Email',
                    contentPadding:
                        EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(30),
                      ),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(30)),
                      borderSide: BorderSide(color: Colors.grey, width: 1),
                    ),
                    focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(30)),
                        borderSide: BorderSide(color: Colors.blue, width: 1))),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              TranslationService.of(context)!.translate("Full Name Sender"),
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
            Padding(
              padding:
                  EdgeInsets.fromLTRB(defaultPadding, 0, defaultPadding, 0),
              child: TextField(
                controller: _FullNameSender,
                textAlign: TextAlign.center,
                onChanged: (value) {
                  FullNameSender = value;
                },
                decoration: InputDecoration(
                    hintText: TranslationService.of(context)!
                        .translate('Enter Full Name'),

                    // hintText: 'Enter Your Email',
                    contentPadding:
                        EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(30),
                      ),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(30)),
                      borderSide: BorderSide(color: Colors.grey, width: 1),
                    ),
                    focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(30)),
                        borderSide: BorderSide(color: Colors.blue, width: 1))),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            MyBotton(
              //color: Colors.blue[800]!,
              colorStart: Color.fromARGB(255, 100, 224, 255),
              colorEnd: Color.fromARGB(255, 31, 148, 243),
              color: [
                Color.fromARGB(255, 100, 224, 255),
                Color.fromARGB(255, 31, 148, 243)
              ],

              title: TranslationService.of(context)!.translate('Send Notify'),
              onPressed: () async {
                // if (user != null) {
                await sendNotfiy(
                    NotificationTitle, NotficationText, "1", FullNameSender);
                _firestor.collection('notification').add(
                  {
                    'title': NotificationTitle,
                    'text': NotficationText,
                    'namesender': FullNameSender,
                    'sender': signedInUser.email,
                    'time': FieldValue.serverTimestamp(),
                    // 'receiver': signedInUser.email,
                    //'isbolt': false,
                    //'time': FieldValue.serverTimestamp(),
                  },
                );
                //   } else {
                //   Navigator.pushNamed(context, WelcomeScreen.screenRoute);
                //   }
              },
              // child: Text("send Notify"),
            ),
            SizedBox(
              height: 20,
            ),
            // MaterialButton(
            //   onPressed: () async {
            //     //await sendNotfiy("title", "body", "1");
            //     await sendNotfiy(NotificationTitle, NotficationText, "1");
            //   },
            //   child: Text("send Notify"),
            // ),

            MyBotton(
                // color: Colors.blue[800]!,
                colorStart: Color.fromARGB(255, 100, 224, 255),
                colorEnd: Color.fromARGB(255, 31, 148, 243),
                color: [
                  Color.fromARGB(255, 100, 224, 255),
                  Color.fromARGB(255, 31, 148, 243)
                ],
                title: TranslationService.of(context)!
                    .translate('Subcribe To Topic'),
                // MaterialButton(
                onPressed: () async {
                  //اذا بدي ارسل اشعار فاي شخص بدي ارسله لازم يكون بتوبك يلي اسمها فراس
//ومن المفروض بعد ما يعمل تسجيل دخول خليه يعمل
//SubcribeToTopic
//باجبار من اجل وقت ارسله داتا تصله لهاد يوزر جميع اشعارات او اشخاص
//يلي عملتلهم اشتراك بتوبك معينة مثل توبك فراس
                  await FirebaseMessaging.instance
                      .subscribeToTopic("notification");

                  // child: Text("SubcribeToTopic"),
                }),
            SizedBox(
              height: 20,
            ),
            MyBotton(
              colorStart: Color.fromARGB(255, 100, 224, 255),
              colorEnd: Color.fromARGB(255, 31, 148, 243),
              color: [
                Color.fromARGB(255, 100, 224, 255),
                Color.fromARGB(255, 31, 148, 243)
              ],

              title: TranslationService.of(context)!
                  .translate('UnSubcribe To Topic'),
              //  MaterialButton(
              onPressed: () async {
                await FirebaseMessaging.instance
                    .unsubscribeFromTopic("notification");
              },
              // child: Text("UnSubcribeToTopic"),
            ),
          ],
        )
        // : MaterialApp(initialRoute: NotifactionServer.screenRoute),
        //  MaterialPageRoute(builder: (context) => const SelectionScreen())
        //Container(child: Action(Navigator.pushNamed(context, WelcomeScreen.screenRoute))),
        // : Column(children: [
        //     Container(
        //       child: Text('PLease Singin'),
        //     ),
        //     MyBotton(
        //         color: Colors.orange[800]!,
        //         title: TranslationService.of(context)!.translate('Sign in'),
        //         //title: 'Sign in',
        //         onPressed: () async {
        //           Navigator.pushNamed(context, WelcomeScreen.screenRoute);
        //         }),
        //   ]),
        );
  }
}
