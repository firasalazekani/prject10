import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:clustr_map_test_1/Screens/chat/welcome_screen.dart';
import 'package:clustr_map_test_1/Screens/home_screen.dart';
import 'package:clustr_map_test_1/Screens/notifaction/notifaction_server.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

final _firestor = FirebaseFirestore.instance;
final _auth = FirebaseAuth.instance;

class NotifactionScreen extends StatefulWidget {
  const NotifactionScreen({super.key});
  static const String screenRoute = 'notifaction_screen';

  @override
  State<NotifactionScreen> createState() => _NotifactionScreenState();
}

class _NotifactionScreenState extends State<NotifactionScreen> {
  @override
  // void initState() {
  //   // TODO: implement initState
  //   if (_auth.currentUser != null) {
  //     if (signedInUser.email == 'firas@test.com') {
  //       print('User From Back : ${signedInUser1.email}');
  //       // Navigator.popAndPushNamed(
  //       Navigator.popAndPushNamed(context, NotifactionServer.screenRoute);
  //     } else {
  //       Navigator.pushNamed(context, WelcomeScreen.screenRoute);
  //     }
  //     super.initState();
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.blue,
        title:
            // Row(
            //  mainAxisAlignment: MainAxisAlignment.center,
            //   children: [
            Text(
          'Message Notifications',
          // textAlign: TextAlign.end,
          style: TextStyle(fontSize: 18),
        ),
        //    ],
        //  ),
        actions: [
          IconButton(
              onPressed: () {
                _auth.signOut();
                //من اجل وقت يعمل تسجيل خروج يعمل الغاء كي لا تصله الاشعارات
                //وقت يعمل تسجيل دخول بنرد بنفعل هذه الميزة
                FirebaseMessaging.instance.unsubscribeFromTopic("notification");

                Navigator.pushNamed(context, HomeScreen.screenRoute);
              },
              icon: Icon(Icons.close)),
          IconButton(
              onPressed: () {
                //_auth.signOut();
                //من اجل وقت يعمل تسجيل خروج يعمل الغاء كي لا تصله الاشعارات
                //وقت يعمل تسجيل دخول بنرد بنفعل هذه الميزة
                // FirebaseMessaging.instance.unsubscribeFromTopic("notification");

                Navigator.popAndPushNamed(context, HomeScreen.screenRoute);
              },
              icon: Icon(Icons.home)),
        ],
        leading: IconButton(
            onPressed: () {
              Navigator.popAndPushNamed(context, HomeScreen.screenRoute);
            },
            icon: Icon(Icons.arrow_back)),
      ),
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            MessageStreamBilder(),
            /*  Container(
              // //width: 100,
              // //لرسم خط مستقيم
              // decoration: BoxDecoration(
              //   border: Border(
              //     top: BorderSide(
              //       color: Colors.orange,
              //       width: 2,
              //     ),
              //   ),
              // ),
              // //----End firas
              // // color: Colors.orange,
              child: Row(
                children: [
                  // Expanded(
                  //   child: TextField(
                  //     style: TextStyle(fontSize: 22),
                  //     controller: messageTextController,
                  //     onChanged: (value) {
                  //       messageText = value;
                  //     },
                  //     decoration: InputDecoration(
                  //         contentPadding: EdgeInsets.symmetric(
                  //             vertical: 20, horizontal: 10),
                  //         hintText: TranslationService.of(context)!
                  //             .translate('Write your Message here'),
                  //         //  hintText: 'Write your Message here',
                  //         border: InputBorder.none),
                  //   ),
                  // ),
                  // // Container(
                  // // SizedBox(
                  // //   width: 20,
                  // // ),
                  Stack(
                    children: [
                      Padding(
                        //padding: const EdgeInsets.fromLTRB(0, 20, 30, 0),
                        padding: const EdgeInsets.only(top: 20),
                        child: IconButton(
                          onPressed: _speechToText.isNotListening
                              ? _startListening
                              : write,
                          // onPressed: (_speechToText.isNotListening) {
                          //  // if (_speechToText.isNotListening == true) {
                          //     _startListening;
                          //    if(_speechToText.isListening) {
                          //     _stopListening;
                          //   }
                          // }
                          //tooltip: 'Listen',
                          //listen();

                          icon: Icon(_speechToText.isNotListening
                              ? Icons.mic_off
                              : Icons.mic),
                          alignment: Alignment.centerLeft,

                          // ),
                        ),
                      ),
                      // Padding(padding: EdgeInsets.only(top: 100.0)),
                      //  ),
                      // Expanded(
                      // child: Stack(
                      //   children: [
                      // child:
                      // Spacer(),
                      ClipPath(
                        clipper: WaveClipperButton(),
                        child: Container(
                          height: 90,
                          width: 120,
                          alignment: Alignment.center,
                          color: Colors.orange[800],
                          child: TextButton(
                            //style: TextStyle(fontSize: 20),
                            onPressed: () {
                              if (messageText != '') {
                                messageTextController.clear();
                                _firestor.collection('message').add(
                                  {
                                    'text': messageText,
                                    //  'text': text,
                                    'sender': signedInUser.email,
                                    'receiver': signedInUser.email,
                                    'isbolt': false,

                                    'time': FieldValue.serverTimestamp(),
                                    // 'receiver': signedInUser,
                                  },
                                );
                                _firestor.collection('notification').add(
                                  {
                                    'text': messageText,
                                    'sender': signedInUser.email,
                                    // 'receiver': signedInUser.email,
                                    //'isbolt': false,
                                    //'time': FieldValue.serverTimestamp(),
                                  },
                                );
                                SearchWord = messageText.toString();
                                SearchWordInList(SearchWord);
                                //  messageTextController.clear();
                                messageText = '';
                              }
                            },
                            child: Icon(
                              Icons.send,
                              color: Colors.white,
                            ),

                            // 'Send',
                            // style: TextStyle(
                            //   color: Colors.blue[800],
                            //   fontWeight: FontWeight.bold,
                            //   fontSize: 18,
                            //),
                          ),
                        ),
                      ),
                      // ),
                      //   ],
                      // ),
                      //  ),
                      // Expanded(
                      //   child:

                      // ),
                    ],
                  ),
                  // Container(
                  //   width: 20,
                  //   height: 64,
                  //   color: Colors.orange[800],
                  // ),
                ],
              ),
            ),*/
            //  FalseRobot,
          ],
        ),
      ),
    );
  }
}

//من اجل الشكل تبع الرسالة المرسلة
class MessageLine extends StatelessWidget {
  const MessageLine({
    this.text,
    this.sender,
    this.namesender,
    this.title,
    // this.isbolt,
    //this.receiver,

    // required this.isMe,
    Key? key,
  }) : super(key: key);
  final String? text;
  final String? title;
  final String? sender;
  final String? namesender;

  //final bool? isbolt;
  // final String? receiver;
  // final bool isMe;
  //final bool isRobot;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(10.0),
      child: Column(
        crossAxisAlignment:
            // isMe ? CrossAxisAlignment.end : CrossAxisAlignment.start,
            CrossAxisAlignment.stretch,
        children: [
          // isMe
          // ?
          Material(
            elevation: 5,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(30),
                bottomLeft: Radius.circular(30),
                bottomRight: Radius.circular(30)),
            // : BorderRadius.only(
            //     topRight: Radius.circular(30),
            //     bottomLeft: Radius.circular(30),
            //     bottomRight: Radius.circular(30),
            //   ),//
            // color: isMe ? Colors.blue[800] : Colors.white,
            color: Colors.white,
            child: Padding(
                padding: const EdgeInsets.symmetric(
                    vertical: 10.0, horizontal: 20.0),
                child: Text(
                  '$namesender \n $text ',
                  style: TextStyle(fontSize: 25, color: Colors.black45),
                  textAlign: TextAlign.center,
                )),
          )
          // : Row(
          //     children: [
          //       CircleAvatar(
          //         radius: 20.0,
          //         backgroundImage: AssetImage('images/Robot.png'),
          //         backgroundColor: Colors.white,
          //       ),
          //       SizedBox(
          //         width: 20,
          //       ),
          //       Expanded(
          //         child: Material(
          //           elevation: 5,
          //           child: Padding(
          //             padding: const EdgeInsets.all(8.0),
          //             child: Text(
          //               '$text',
          //               style: TextStyle(
          //                 fontSize: 20,
          //                 color: isMe ? Colors.white : Colors.black45,
          //               ),
          //             ),
          //           ),
          //           borderRadius: BorderRadius.only(
          //             topRight: Radius.circular(30),
          //             bottomLeft: Radius.circular(30),
          //             bottomRight: Radius.circular(30),
          //           ),
          //         ),
          //       ),
          //   ],
          //  ),
        ],
      ),
    );
  }
}

//من اجل بناء ليييست الرسائل
class MessageStreamBilder extends StatelessWidget {
  //const MyWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: _firestor.collection('notification').orderBy('time').snapshots(),
      builder: (context, snapshot) {
        List<MessageLine> messageWidgets = [];
        if (!snapshot.hasData) {
          return Center(
            child: CircularProgressIndicator(
              backgroundColor: Colors.blue,
            ),
          );
        }
        final messages = snapshot.data!.docs.reversed;
        for (var message in messages) {
          final messageTitle = message.get('title');
          final messageText = message.get('text');
          final messageSender = message.get('sender');
          final currentUser = signedInUser.email;
          final messageNameSender = message.get('namesender');
          //  final messageReceiver = message.get('receiver');

          final messageWidget = MessageLine(
            title: messageTitle,
            text: messageText,
            sender: messageSender,
            namesender: messageNameSender,
            // isMe: currentUser == messageSender,
            //  isbolt: messageIsBolt,
            //  receiver: messageReceiver,
          );
          // if (currentUser == messageSender) {
          messageWidgets.add(messageWidget);
          //  }
          // if (messageIsBolt == true && messageReceiver == currentUser) {
          //   messageWidgets.add(messageWidget);
          // }
        }
        return Expanded(
          child: ListView(
            reverse: true,
            padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
            children: messageWidgets,
          ),
        );
      },
    );
  }
}
